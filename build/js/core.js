function setModified(_modified)
{
    modified = _modified;

    if(modified)
    {
        window.onbeforeunload = function() { return "You have unsaved changes. Are you sure you want to navigate away?"; };
    }
    else
    {
        window.onbeforeunload = null;
    }
}
