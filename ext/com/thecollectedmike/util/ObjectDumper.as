package com.thecollectedmike.util 
{
	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.DisplayObject;
	import flash.display.MovieClip;

	/**
	 * @author mikej
	 */
	public class ObjectDumper 
	{		
		private static var _logger : ILogger = LoggerFactory.getClassLogger(ObjectDumper);

		public static function dumpClip(displayObject : DisplayObject, indent : uint = 0) : void
		{
			var indentString : String = "";
			for(var j : uint = 0;j < indent;j++)
			{
				indentString += " ";
			}
			_logger.debug(indentString + displayObject.name);
			if(displayObject is MovieClip)
			{
				var clip : MovieClip = (displayObject as MovieClip);
				for(var i : uint = 0;i < clip.numChildren;i++)
				{
					ObjectDumper.dumpClip(clip.getChildAt(i), indent + 1);
				}
			}
		}
	}
}
