package  
{
	import it.utag.controller.AppController;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.system.Security;

	/**
	 * @author mikej
	 */
	[SWF(width="955", height="725", backgroundColor="#ffffff")]

	public class Utagit extends Sprite 
	{
		private var _appController : AppController;

		public function Utagit()
		{
			Security.allowDomain("www.youtube.com");
			
			this._appController = new AppController(this);
			this._appController.addEventListener(Event.INIT, this.handleInit);
			this._appController.init();
		}

		private function handleInit(event : Event) : void
		{
			
			this._appController.removeEventListener(Event.INIT, this.handleInit);
			this._appController.start();
		}
	}
}
