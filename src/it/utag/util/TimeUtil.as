package it.utag.util 
{

	/**
	 * @author mikej
	 */
	public class TimeUtil 
	{
		/**
		 * time: Position in milliseconds
		 * 
		 * Returns in the form HH:MM:ss.mm
		 */
		public static function formatTimePosition(time : Number) : String
		{
			// Hours
			var hours : uint = Math.floor(time / 1000 / 60 / 60);
			time -= (hours * 60 * 60 * 1000);
			var mins : uint = Math.floor(time / 1000 / 60);
			time -= (mins * 60 * 1000);
			var secs : uint = Math.floor(time / 1000);
			time -= (secs * 1000);
			var millis : uint = time;
			
			return 	(hours > 0 ? hours+":" : "") + 
					(mins > 0 ? mins+":" : "") + 
					 secs + 
					(millis > 0 ? "."+millis : "");
			
		}
	}
}
