package it.utag.service 
{
	import flash.net.URLVariables;

	/**
	 * @author mikej
	 */
	public interface IService 
	{
		function getURL() : String;
		function buildResponse(data : Object) : Response;
		function buildRequest() : URLVariables;
		
		function get response() : Response;
		function set response(response : Response) : void;
	}
}
