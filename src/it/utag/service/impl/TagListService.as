package it.utag.service.impl 
{
	import it.utag.model.AppState;
	import it.utag.model.Media;
	import it.utag.model.Tag;
	import it.utag.model.TagInstance;
	import it.utag.model.User;
	import it.utag.service.IService;
	import it.utag.service.Response;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.net.URLVariables;
	import flash.utils.Dictionary;

	/**
	 * @author mikej
	 */
	public class TagListService implements IService 
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(TagListService);
		
		private var _media : Media;
		private var _response : Response;
		
		namespace utagit = "http://ns.utag.it/";

		public function getURL() : String
		{
			return "http://api.utag.it/services/v0/taggingevents/search/";
		//	return "http://api.utag.it/services/v0/media/tag3list/";
		}

		public function buildResponse(data : Object) : Response
		{
			var response : Response = new Response();
			var responseXML : XML = new XML(data + "");
			if(data == "No tags found matching this query")
			{
				response.data = [];
				response.success = true;
				return response;
			}
			
			/*
			 *   <tag>
			<uri>http://www.youtube.com/watch?v=u1zgFlCw8Aw</uri>
			<instant>4923</instant>
			<predicate>ssss</predicate>
			<tag_name>fff</tag_name>
			<relevance>0.278409</relevance>
			<added_on>2010-02-02 17:12:15.493894+01</added_on>
			<dod>dot</dod>
			<action>create</action>
			</tag>
			 */
			use namespace utagit;
			var taggingInstances : Array = [];
	
			var maxDate : Dictionary = new Dictionary();
			var minDate : Dictionary = new Dictionary();
			var tags : Dictionary = new Dictionary();
			
			for each(var tagXML : XML in responseXML.tags..tag)
			{
				var instant : uint = parseInt(tagXML.instant.text());
				var predicate : String = tagXML.predicate.text();
				var tag_name : String = tagXML.tag_name.text();
				var relevance : Number = parseFloat(tagXML.relevance.text());
				var added_on : String = tagXML.added_on.text();
				var action : String = tagXML.action.text();
				var user : String = tagXML.user.text();
				if(user == null) 
				{ 					
					user = User.ANON; 
				}
				
				// A few validation checks.
				if(isNaN(instant))
					continue;
					
				if(isNaN(relevance))
					continue;
				
				if(tag_name == "")	
					continue;
				
				var taggingInstance : TagInstance = new TagInstance();
				taggingInstance.instant = instant;
				taggingInstance.relevance = relevance;
				taggingInstance.media = AppState.getInstance().media;
				taggingInstance.creator = user;
				
				var tag : Tag;
				if(tags[tag_name])
				{
					tag = tags[tag_name];
				}
				else
				{
					tag = new Tag();
					tags[tag_name] = tag;
				}
			
				tag.predicate = predicate;
				tag.object = tag_name;
				
				taggingInstance.tag = tag;
				
				if(action == "create")
				{
					// 2009-06-02 13:00:54.990236+02
					var dateRegExp : RegExp = /([0-9]{4})\-([0-9]{2})\-([0-9]{2})\s+([0-9][0-9]):([0-9][0-9]):([0-9][0-9])\.([0-9]+)([\+\-])([0-9][0-9])/;
					
					var matches : Array = added_on.match(dateRegExp);
					
					var ye : uint = parseInt(matches[1]);
					var mo : uint = parseInt(matches[2]);
					var da : uint = parseInt(matches[3]);
					var ho : uint = parseInt(matches[4]);
					var mi : uint = parseInt(matches[5]);
					var se : uint = parseInt(matches[6]);
					var ms : uint = parseInt(matches[7]);
					var hrs : uint = parseInt(matches[9]);
					
					var dateStr : String = (mo + "/" + da + "/" + ye + " " + ho + ":" + mi + ":" + se + " GMT" + matches[8] + hrs + "00");
					var millis : Number = Date.parse(dateStr);
					
					
					var date : Date = new Date();
					date.setTime(millis);
					
					if(!maxDate[tag_name] || maxDate[tag_name] < millis)
					{
						maxDate[tag_name] = millis;
						tag.modified_on = date;
					}
					if(!minDate[tag_name] || minDate[tag_name] > millis)
					{
						minDate[tag_name] = millis;
						tag.added_on = date;
					}
					
					taggingInstances.push(taggingInstance);
				}
			}

			response.data = taggingInstances;
			response.success = true;
			return response;
		}

		public function buildRequest() : URLVariables
		{
			var data : URLVariables = new URLVariables();
			data["uri"] = this.media.url;
			data["format"] = "xml";
			data["display_deleted"] = "true";
			//		data["userid"] = AppState.getInstance().user.username;
			return data;
		}

		public function get media() : Media
		{
			return _media;
		}

		public function set media(media : Media) : void
		{
			_media = media;
		}

		public function get response() : Response
		{
			return this._response;
		}

		public function set response(response : Response) : void
		{
			this._response = response;
		}
	}
}
