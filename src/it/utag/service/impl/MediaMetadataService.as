package it.utag.service.impl 
{
	import it.utag.model.Media;
	import it.utag.service.IService;
	import it.utag.service.Response;

	import flash.net.URLVariables;

	/**
	 * @author moj
	 */
	public class MediaMetadataService implements IService
	{
		private var _response : Response;
		private var _id : String;
		namespace media = "http://search.yahoo.com/mrss/";
		
		
		public function MediaMetadataService()
		{
		}
		
		public function getURL() : String
		{
			return "http://gdata.youtube.com/feeds/api/videos/"+_id;
		}
		
		public function buildResponse(data : Object) : Response
		{
			this._response = new Response();
			if(data == "Invalid id")
			{
				this._response.success = false;
				return _response;
			}
			
			
			use namespace media;
			var responseXML : XML = new XML(data+"");
			var media : Media = new Media();
			media.title = responseXML.group.title.text();
			var candidates : XMLList = responseXML.group.thumbnail.(@height=='90');
			var cand2 : XMLList = candidates.(@width='120');
			media.thumbURL = cand2[0].@url;
			media.url = "http://www.youtube.com/v/"+_id;
			this._response.data = media;
			this._response.success = true;
			return this._response;
			
		}
		
		public function buildRequest() : URLVariables
		{
			return new URLVariables();
		}
		
		public function get response() : Response
		{
			return _response;
		}
		
		public function set response(response : Response) : void
		{
			this._response = response;
		}
		
		public function get id() : String
		{
			return _id;
		}
		
		public function set id(id : String) : void
		{
			_id = id;
		}
	}
}
