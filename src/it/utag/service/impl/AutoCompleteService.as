package it.utag.service.impl 
{
	import com.thecollectedmike.util.JSON;
	import it.utag.service.IService;
	import it.utag.service.Response;

	import flash.net.URLVariables;

	/**
	 * @author mikej
	 */
	public class AutoCompleteService implements IService 
	{
		private var _predicate : String;
		private var _tag_name : String;
		private var _response : Response;

		public function getURL() : String
		{
			return "http://api.utag.it/services/v0/tag2/list";
		}
		
		public function buildResponse(data : Object) : Response
		{
			this._response = new Response();
			this._response.success = true;
			var json : JSON = new JSON();
			var obj : Object = json.parse(data+"");
			
			this._response.data = obj["json_taglist"];
			return this._response;
		}
		
		public function buildRequest() : URLVariables
		{
			var variables : URLVariables  = new URLVariables();
			variables["tag_name"] = tag_name;
			variables["predicate"] = predicate;
			variables["partial"] = 1;
			return variables;
		}
		
		public function get response() : Response
		{
			return this._response;
		}
		
		public function set response(response : Response) : void
		{
			this._response = response;
		}
		
		public function get predicate() : String
		{
			return _predicate;
		}
		
		public function set predicate(predicate : String) : void
		{
			_predicate = predicate;
		}
		
		public function get tag_name() : String
		{
			return _tag_name;
		}
		
		public function set tag_name(tag_name : String) : void
		{
			_tag_name = tag_name;
		}
	}
}
