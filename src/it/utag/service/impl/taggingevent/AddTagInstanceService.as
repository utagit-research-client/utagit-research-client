package it.utag.service.impl.taggingevent 
{
	import it.utag.service.Response;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.net.URLVariables;

	/**
	 * @author mikej
	 */
	public class AddTagInstanceService extends BaseTagInstanceService 
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(AddTagInstanceService);
		private var _result_id : int;
		
		public override function getURL() : String
		{
			return "http://api.utag.it/services/v0/taggingevents/add_or_update/";
		}
		
		public override function buildResponse(data : Object) : Response
		{
			var response : Response = new Response();
			_logger.debug("Add result: "+data);
			response.data = parseInt(""+data);
			response.success = true;
			return response;
		}
		
		public override function buildRequest() : URLVariables
		{
			var request : URLVariables = super.buildRequest();
			request["userid"] = tagInstance.creator;
			return request;	
		}
		
		public function get result_id() : int
		{
			return _result_id;
		}
	}
}
