package it.utag.service.impl.taggingevent 
{

	/**
	 * @author mikej
	 */
	public class SearchTagInstanceService extends BaseTagInstanceService 
	{
		public function SearchTagInstanceService()
		{
		}
		
		public override function getURL() : String
		{
			return "http://api.utag.it/services/v0/taggingevents/search/";
		}
	}
}
