package it.utag.service.impl.taggingevent 
{
	import it.utag.service.Response;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.net.URLVariables;

	/**
	 * @author mikej
	 */
	public class MoveTagInstanceService extends BaseTagInstanceService 
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(MoveTagInstanceService);
		private var _result_id : int;
		private var _instant : int;
		
		public override function getURL() : String
		{
			return "http://api.utag.it/services/v0/taggingevents/move/";
		}
		
		public override function buildResponse(data : Object) : Response
		{
			var response : Response = new Response();
			_logger.debug("Move result: "+data);
			response.data = parseInt(""+data);
			
			response.success = true;
			return response;
		}
		
		public override function buildRequest() : URLVariables
		{	
			var data : URLVariables = super.buildRequest();

			data["new_timecode"] = _instant;
			data["userid"] = tagInstance.creator;
			return data;
		}
		
		public function get result_id() : int
		{
			return _result_id;
		}
		
		public function set instant(instant : int) : void
		{
			_instant = instant;
		}
	}
}
