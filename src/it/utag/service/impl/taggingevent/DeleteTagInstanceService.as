package it.utag.service.impl.taggingevent 
{
	import it.utag.service.Response;

	import flash.net.URLVariables;

	/**
	 * @author mikej
	 */
	public class DeleteTagInstanceService extends BaseTagInstanceService 
	{

		public override function getURL() : String
		{
			return "http://api.utag.it/services/v0/taggingevents/delete/";
		}

		public override function buildResponse(data : Object) : Response
		{
			var response : Response = new Response();
			if(data == "already deleted")
			{
				response.success = false;	
			}
			else
			{
				response.success = true;
			}
			return response;
		}
		
		public override function buildRequest() : URLVariables
		{
			var request : URLVariables = super.buildRequest();
			request["userid"] = tagInstance.creator;
			return request;	
		}

	}
}
