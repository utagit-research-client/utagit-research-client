package it.utag.service.impl.taggingevent 
{
	import it.utag.model.AppState;
	import it.utag.model.TagInstance;
	import it.utag.service.IService;
	import it.utag.service.Response;

	import flash.errors.IllegalOperationError;
	import flash.net.URLVariables;

	/**
	 * @author moj
	 */
	public class BaseTagInstanceService implements IService
	{
		private var _tagInstance : TagInstance;
		private var _response : Response;

		public function getURL() : String
		{
			throw new IllegalOperationError("getURL() must be overridden");
		}

		public function buildResponse(data : Object) : Response
		{
			throw new IllegalOperationError("buildResponse(data) must be overridden");
		}

		public function buildRequest() : URLVariables
		{	
			var data : URLVariables = new URLVariables();

			data["uri"] = _tagInstance.media.url;
			data["timecode"] = _tagInstance.instant;
			data["predicate_name"] = _tagInstance.tag.predicate;
			data["tag_name"] = _tagInstance.tag.object;
			data["dod"] = _tagInstance.dod;
			data["relevance"] = _tagInstance.relevance;
			data["userid"] = AppState.getInstance().user.username;
			
			return data;
		}

		public function set tagInstance(tagInstance : TagInstance) : void
		{
			_tagInstance = tagInstance;
		}

		public function get response() : Response
		{
			return this._response;
		}

		public function set response(response : Response) : void
		{
			this._response = response;
		}
		
		public function get tagInstance() : TagInstance
		{
			return _tagInstance;
		}
	}
}
