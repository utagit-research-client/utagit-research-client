package it.utag.service.impl 
{
	import it.utag.model.AppState;
	import it.utag.model.Datum;
	import it.utag.model.Media;
	import it.utag.service.IService;
	import it.utag.service.Response;

	import flash.net.URLVariables;
	import flash.utils.Dictionary;

	/**
	 * @author moj
	 */
	public class SearchMediaService implements IService 
	{
		private var _tagName : String;
		private var _response : Response;
		
		namespace utagit = "http://ns.utag.it/";

		public function SearchMediaService()
		{
		}

		public function getURL() : String
		{
			return "http://api.utag.it/services/v0/taggingevents/search/";
		}

		public function buildResponse(data : Object) : Response
		{
			this._response = new Response();
			var responseXML : XML = new XML(data + "");
			
			use namespace utagit;
			var uris : Array = [];
			
			for each(var tagXML : XML in responseXML.tags..tag)
			{
				var tag_name : String = tagXML.tag_name.text();
				var media_uri : String = tagXML.uri.text();
			
				if(tag_name == _tagName && uris.indexOf(media_uri) == -1)
				{
					uris.push(media_uri);
				}			
			}
			
			var results : Array = [];
			
			var  youTubeMatch : RegExp = /^(.+)youtube\.com(.+)v[\/=](.+)$/;
			
			var taken : Dictionary = new Dictionary();
			for each(var uri : String in uris)
			{
				var datum : Datum = new Datum();
				datum.name = uri;
				var matches : Array = uri.match(youTubeMatch);
				if(taken[matches[3]])
				{
					continue;
				}
				
				datum.hidden = matches[3];
				datum.type = Media;	
				results.push(datum);
				
				taken[matches[3]] = true;
			}
			_response.data = results;			
			_response.success = true;
			
			return _response;
		}

		public function buildRequest() : URLVariables
		{	
			var data : URLVariables = new URLVariables();
			data["tag_name"] = _tagName;
			data["predicate_name"] = "is_a";
			data["format"] = "xml";
	//		data["userid"] = AppState.getInstance().user.username;
			return data;
		}

		public function get tagName() : String
		{
			return _tagName;
		}

		public function set tagName(tagName : String) : void
		{
			_tagName = tagName;
		}

		public function get response() : Response
		{
			return _response;
		}

		public function set response(response : Response) : void
		{
			_response = response;
		}
	}
}
