package it.utag.service.impl 
{
	import it.utag.model.AppState;
	import it.utag.model.Tag;
	import it.utag.service.IService;
	import it.utag.service.Response;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.net.URLVariables;

	/**
	 * @author moj
	 */
	public class SaveSnapshotService implements IService 
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(SaveSnapshotService);
		private var _tag : Tag;
		private var _response : Response;
		
		public function getURL() : String
		{
			return "http://api.utag.it/services/v0/snapshots/save/";
		}
		
		public function buildResponse(data : Object) : Response
		{
			this._response = new Response();
			_logger.debug("Snapshot data: "+data);
			
			return this._response;
		}
		
		public function buildRequest() : URLVariables
		{
			
			var data : URLVariables = new URLVariables();
			data["tag_name"] = _tag.object;
			data["predicate_name"] = _tag.predicate;
			data["format"] = "xml";
			data["uri"] = AppState.getInstance().media.url;
			return data;
		}
		
		public function get response() : Response
		{
			return this._response;
		}
		
		public function set response(response : Response) : void
		{
			this._response = response;
		}
		
		public function get tag() : Tag
		{
			return _tag;
		}
		
		public function set tag(tag : Tag) : void
		{
			_tag = tag;
		}
	}
}
