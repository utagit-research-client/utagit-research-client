package it.utag.service 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	/**
	 * @author moj
	 */
	public class BaseService extends EventDispatcher
	{
		private var _request : URLRequest;
		private var _loader : URLLoader;
		private var _response : Response;

		public function sendRequest(url : String, data : Object) : void
		{
			this._request = new URLRequest(url);	

			this._request.data = data;
			this._loader = new URLLoader();
			this._loader.addEventListener(Event.COMPLETE, this.handleRequestComplete);
			
			this._loader.load(this._request);
		}

		protected function handleRequestComplete(event : Event) : void
		{
			this._loader.removeEventListener(Event.COMPLETE, this.handleRequestComplete);
			this.requestFinished(this._loader.data);
			this.dispatchEvent(new Event(Event.COMPLETE));
		}

		protected function requestFinished(data : Object) : void
		{
		}

		public function get response() : Response
		{
			return _response;
		}

		public function set response(response : Response) : void
		{
			_response = response;
		}
	}
}
