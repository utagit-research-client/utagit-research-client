package it.utag.service 
{

	/**
	 * @author moj
	 */
	public class Response 
	{
		private var _success : Boolean;
		private var _data : Object;
		
		public function Response()
		{
			this._success = false;
		}
		
		public function get success() : Boolean
		{
			return _success;
		}
		
		public function set success(success : Boolean) : void
		{
			_success = success;
		}
		
		public function get data() : Object
		{
			return _data;
		}
		
		public function set data(data : Object) : void
		{
			_data = data;
		}
	}
}
