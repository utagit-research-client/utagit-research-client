package it.utag.service 
{
	import flash.events.Event;

	/**
	 * @author moj
	 */
	public class ServiceEvent extends Event 
	{

		public static const SERVICE_COMPLETE : String = "serviceComplete";
		
		private var _service : IService;

		public function ServiceEvent(type : String, service : IService, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			this._service = service;
			super(type, bubbles, cancelable);
		}
		
		public override function clone() : Event
		{
			return new ServiceEvent(type, service, bubbles, cancelable);
		}
		
		public function get service() : IService
		{
			return _service;
		}
	}
}
