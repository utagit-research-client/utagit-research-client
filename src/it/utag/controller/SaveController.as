package it.utag.controller 
{
	import it.utag.controller.event.SaveEvent;
	import it.utag.controller.event.TagInstanceEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.controller.manager.ServiceManager;
	import it.utag.model.AppState;
	import it.utag.model.ChangeItem;
	import it.utag.model.TagInstance;
	import it.utag.service.ServiceEvent;
	import it.utag.service.impl.taggingevent.AddTagInstanceService;
	import it.utag.service.impl.taggingevent.DeleteTagInstanceService;
	import it.utag.service.impl.taggingevent.MoveTagInstanceService;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.events.Event;
	import flash.external.ExternalInterface;

	/**
	 * @author moj
	 */
	public class SaveController  extends Controller
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(SaveController);
		private var _saving : Boolean;
		private var _addTagInstanceService : AddTagInstanceService;
		private var _currentChangeItem : ChangeItem;
		private var _deleteTagInstanceService : DeleteTagInstanceService;
		private var _updateTagInstanceService : AddTagInstanceService;
		private var _moveTagInstanceService : MoveTagInstanceService;

		private static var instance : SaveController;

		public function SaveController()
		{
			this._saving = false;
			
			ServiceManager.getInstance().addEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleServiceComplete);
			this._addTagInstanceService = new AddTagInstanceService();
			this._deleteTagInstanceService = new DeleteTagInstanceService();
			this._updateTagInstanceService = new AddTagInstanceService();
			this._moveTagInstanceService = new MoveTagInstanceService();
		}

		public override function init() : void
		{
			NotificationManager.getInstance().addEventListener(SaveEvent.STATE_DIRTY, this.handleDirtyState);
			this.dispatchEvent(new Event(Event.INIT));
		}

		public static function getInstance() : SaveController
		{
			if(!instance)
			{
				instance = new SaveController();	
			}
			return instance;
		}

		private function handleDirtyState(event : SaveEvent) : void
		{
			// If we're already in the middle of saving something,
			// there's no need to start the timer.
			if(!this._saving)
			{
				if(ExternalInterface.available)
				{
					ExternalInterface.call("setModified", true);
				}
				
				saveNextItem();
			}
		}

		private function saveNextItem() : void
		{
			_logger.debug("Save stack: " + AppState.getInstance().changeItems);
			if(AppState.getInstance().changeItems.length == 0)
			{
				if(ExternalInterface.available)
				{
					ExternalInterface.call("setModified", false);
				}
				this._saving = false;
				AppController.getInstance().hideSaving();
			}
			else
			{
				this._saving = true;
				AppController.getInstance().showSaving();
				if(ExternalInterface.available)
				{
					ExternalInterface.call("setModified", true);
				}
				
				this._currentChangeItem = AppState.getInstance().changeItems.shift();
			
				_currentChangeItem.newTagInstance.tag.modified_on = new Date();
				switch(_currentChangeItem.type)
				{
					case ChangeItem.TYPE_UPDATE:
						_updateTagInstanceService.tagInstance = _currentChangeItem.newTagInstance.clone();
						_updateTagInstanceService.tagInstance.instant = _currentChangeItem.oldTagInstance.instant;
						
						ServiceManager.getInstance().sendRequest(_updateTagInstanceService);
						break;
					case ChangeItem.TYPE_ADD:
						_addTagInstanceService.tagInstance = _currentChangeItem.newTagInstance;
						_currentChangeItem.newTagInstance.tag.added_on = new Date();
						ServiceManager.getInstance().sendRequest(_addTagInstanceService);
						break;
					case ChangeItem.TYPE_DELETE:
						// In this case we want to delete the -old- instance - as the deleted vertex
						// may be at a different instant.
						_deleteTagInstanceService.tagInstance = _currentChangeItem.oldTagInstance;
						ServiceManager.getInstance().sendRequest(_deleteTagInstanceService);
						break;	
				}
			}
		}

		private function handleServiceComplete(event : ServiceEvent) : void
		{
			if(event.service == _updateTagInstanceService)
			{
				_moveTagInstanceService.tagInstance = _currentChangeItem.newTagInstance.clone();
				_moveTagInstanceService.instant = _currentChangeItem.newTagInstance.instant;
				_moveTagInstanceService.tagInstance.instant = _currentChangeItem.oldTagInstance.instant;
				ServiceManager.getInstance().sendRequest(_moveTagInstanceService);
			}
			else if(event.service == this._deleteTagInstanceService || event.service == this._addTagInstanceService || event.service == this._moveTagInstanceService)
			{
				_logger.debug("Saved");
				if(event.service.response.success)
				{
					saveNextItem();
				}
				else
				{
					AppState.getInstance().changeItems.unshift(_currentChangeItem);
				}
			}
		}

		/*
		 * For simplicity, this simply pushes on a change to invert the
		 * last change. This could be made more efficient by removing changes
		 * that haven't yet been saved from the queue.
		 */
		public function undo() : void 
		{
			var appState : AppState = AppState.getInstance();
			// Get the last change item
			var topmost : ChangeItem = appState.lastChangeItem;
			
			// Update system state.
			if(topmost.type == ChangeItem.TYPE_ADD)
			{
				AppState.getInstance().removeTagInstance(topmost.newTagInstance);
			}
			else if(topmost.type == ChangeItem.TYPE_DELETE)
			{
				AppState.getInstance().addTagInstance(topmost.oldTagInstance);
			}
			else if(topmost.type == ChangeItem.TYPE_UPDATE)
			{
				// Switch the relevances
				var updatedInstance : TagInstance = AppState.getInstance().getTagInstanceAt(topmost.newTagInstance.tag, topmost.newTagInstance.instant);
				updatedInstance.instant = topmost.oldTagInstance.instant;
				updatedInstance.relevance = topmost.oldTagInstance.relevance;
				NotificationManager.getInstance().dispatchEvent(new TagInstanceEvent(TagInstanceEvent.TAGINSTANCE_UPDATED, updatedInstance));
			}
				
			// Push on the inverse so it gets
			// saved back into the system.
			var replacement : ChangeItem = topmost.clone();
			
			// Work out the inverse action.
			if(topmost.type == ChangeItem.TYPE_ADD)
			{
				replacement.type = ChangeItem.TYPE_DELETE;
				replacement.oldTagInstance = replacement.newTagInstance;
			}
				else if(topmost.type == ChangeItem.TYPE_DELETE)
			{
				replacement.type = ChangeItem.TYPE_ADD;
				replacement.newTagInstance = replacement.oldTagInstance;
			}
			else if(topmost.type == ChangeItem.TYPE_UPDATE)
			{
				replacement.type = ChangeItem.TYPE_UPDATE;
				var oldRelevance : Number = topmost.oldTagInstance.relevance;
				var oldInstant : Number = topmost.oldTagInstance.instant;
				
				replacement.oldTagInstance.relevance = topmost.newTagInstance.relevance;
				replacement.newTagInstance.relevance = oldRelevance;
				
				
				replacement.oldTagInstance.instant = topmost.newTagInstance.instant;
				replacement.newTagInstance.instant = oldInstant;
			}
			_logger.debug("Added change item " + replacement.type);
			AppState.getInstance().addChangeItem(replacement);
		}
	}
}
