package it.utag.controller.event 
{
	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class VideoEvent extends Event 
	{
		public static const POSITION_CHANGED : String = "positionChanged";
		public static const PAUSED : String = "paused";
		public static const RESUMED : String = "resumed";
		public static const LOADED : String = "loaded";
		
		private var _position : uint;

		public function VideoEvent(type : String, position : uint = 0, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._position = position;
		}
		
		public function get position() : uint
		{
			return _position;
		}
		
		public function set position(position : uint) : void
		{
			_position = position;
		}
		
		public override function clone() :Event
		{
			return new VideoEvent(type, position, bubbles, cancelable);
		}
	}
}
