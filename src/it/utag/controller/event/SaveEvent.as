package it.utag.controller.event 
{
	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class SaveEvent extends Event
	{		
		public static const STATE_DIRTY : String = "stateDirty";

		public function SaveEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
		}

		
		public override function clone() : Event
		{
			return new SaveEvent(type, bubbles, cancelable);
		}
	}
}
