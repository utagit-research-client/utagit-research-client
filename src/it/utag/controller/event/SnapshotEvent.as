package it.utag.controller.event 
{
	import it.utag.model.Snapshot;

	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class SnapshotEvent extends Event
	{		
		public static const SNAPSHOT_ADDED : String = "snapshotAdded";

		private var _snapshot : Snapshot;

		public function SnapshotEvent(type : String, snapshot : Snapshot, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._snapshot = snapshot;
		}


		public override function clone() : Event
		{
			return new SnapshotEvent(type, snapshot, bubbles, cancelable);
		}
		
		public function get snapshot() : Snapshot
		{
			return _snapshot;
		}
		
		public function set snapshot(snapshot : Snapshot) : void
		{
			_snapshot = snapshot;
		}
	}
}
