package it.utag.controller.event 
{
	import it.utag.model.Tag;

	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class TagEvent extends Event
	{		
		public static const TAG_ADDED : String = "tageAdded";
		public static const TAG_REMOVED : String = "tagRemoved";

		private var _tag : Tag;
		private var _isNew : Boolean;

		public function TagEvent(type : String, tag : Tag, isNew : Boolean = false, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._tag = tag;
			this._isNew = isNew;
		}

		public function get tag() : Tag
		{
			return _tag;
		}

		public function set tag(tag : Tag) : void
		{
			_tag = tag;
		}

		public override function clone() : Event
		{
			return new TagEvent(type, tag, isNew, bubbles, cancelable);
		}
		
		public function get isNew() : Boolean
		{
			return _isNew;
		}
	}
}
