package it.utag.controller.event 
{
	import flash.events.Event;
	
	/**
	 * @author mikej
	 */
	public class UIEvent extends Event 
	{
		public static const OVERLAY_SHOWN : String = "overlayShown";
		public static const OVERLAY_HIDDEN : String = "overlayHidden";
		public static const TAGCLOUD_DRAGSTART : String = "tagcloudDragStart";
		public static const TAGCLOUD_DRAGEND : String = "tagcloudDragEnd";
		private var _modal : Boolean;

		public function UIEvent(type : String, modal : Boolean = false, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._modal = modal;
		}
		
		public function get modal() : Boolean
		{
			return _modal;
		}
		
		public override function clone() : Event
		{
			return new UIEvent(type, modal, bubbles, cancelable);
		}
	}
}
