package it.utag.controller.event 
{
	import it.utag.model.Tag;

	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class WaveEvent extends Event 
	{
		public static const WAVE_ADDED : String = "waveAdded";
		public static const WAVE_REMOVED : String = "waveRemoved";
		public static const WAVE_SELECTED : String = "waveSelected";
		
		private var _tag : Tag;

		public function WaveEvent(type : String, tag : Tag, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._tag = tag;
		}
		
		public function get tag() : Tag
		{
			return _tag;
		}
		
		public function set tag(tag : Tag) : void
		{
			_tag = tag;
		}
		
		public override function clone() :Event
		{
			return new WaveEvent(type, tag, bubbles, cancelable);
		}
	}
}
