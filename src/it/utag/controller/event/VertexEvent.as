package it.utag.controller.event 
{
	import it.utag.view.control.VertexHandle;

	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class VertexEvent extends Event 
	{
		public static const VERTEX_MOVED : String = "vertexMoved";
		public static const VERTEX_RELEASED : String = "vertexRelease";
		public static const VERTEX_GRABBED : String = "vertexGrabbed";
		private var _vertexHandle : VertexHandle;

		public function VertexEvent(type : String, vertexHandle : VertexHandle, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._vertexHandle = vertexHandle;
		}
		
		public override function clone() : Event
		{
			return new VertexEvent(type, _vertexHandle, bubbles, cancelable);
		}
		
		public function get vertexHandle() : VertexHandle
		{
			return _vertexHandle;
		}
	}
}
