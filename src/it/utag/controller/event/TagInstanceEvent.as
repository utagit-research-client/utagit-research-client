package it.utag.controller.event 
{
	import it.utag.model.TagInstance;

	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class TagInstanceEvent extends Event 
	{
		public static const TAGINSTANCE_ADDED : String = "tagInstanceAdded";
		public static const TAGINSTANCE_UPDATED : String = "tagInstanceUpdated";
		public static const TAGINSTANCE_REMOVED : String = "tagInstanceRemoved";
		
		private var _tagInstance : TagInstance;

		public function TagInstanceEvent(type : String, tagInstance : TagInstance, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._tagInstance = tagInstance;
		}
		
		public function get tagInstance() : TagInstance
		{
			return _tagInstance;
		}
		
		public function set tagInstance(tagInstance : TagInstance) : void
		{
			_tagInstance = tagInstance;
		}
		
		public override function clone() :Event
		{
			return new TagInstanceEvent(type, tagInstance, bubbles, cancelable);
		}
	}
}
