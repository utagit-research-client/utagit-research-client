package it.utag.controller.event 
{
	import it.utag.model.Tag;

	import flash.events.Event;

	/**
	 * @author mikej
	 */
	public class WaveTagEvent extends Event 
	{
		public static const WAVETAG_SELECTED : String = "waveTagSelected";
		public static const WAVETAG_SNAPSHOT_TOGGLE : String = "waveTagSnapshotToggle";
		
		private var _tag : Tag;

		public function WaveTagEvent(type : String, tag : Tag, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			this._tag = tag;
		}
		
		public function get tag() : Tag
		{
			return _tag;
		}
		
		public function set tag(tag : Tag) : void
		{
			_tag = tag;
		}
		
		public override function clone() :Event
		{
			return new WaveTagEvent(type, tag, bubbles, cancelable);
		}
	}
}
