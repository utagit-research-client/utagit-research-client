package it.utag.controller.dialog 
{
	import it.utag.controller.AppController;
	import it.utag.controller.Controller;
	import it.utag.controller.event.UIEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.gfxSwitchDialog;
	import it.utag.model.AppState;

	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author mikej
	 */
	public class SwitchController extends Controller 
	{
		private var _dialogClip : gfxSwitchDialog;
		private var _usernameField : TextField;
		private var _switchButton : SimpleButton;
		private var _closeButton : MovieClip;

		public function SwitchController(dialogPanel : MovieClip)
		{
			this._dialogClip = new gfxSwitchDialog();
			dialogPanel.addChild(this._dialogClip);
			this._dialogClip.visible = false;
			this._dialogClip.x = (dialogPanel.stage.width - _dialogClip.width) / 2;
			this._dialogClip.y = (dialogPanel.stage.height - _dialogClip.height) / 2;
			
			this._usernameField = this._dialogClip.txtUsername;
			this._switchButton = this._dialogClip.btnSwitch;
			this._closeButton = this._dialogClip.btnClose;
			this._closeButton.buttonMode = true;
			
			super();
		}

		public override function init() : void
		{
			this.dispatchEvent(new Event(Event.INIT));
		}

		public function show() : void 
		{
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_SHOWN, true));
			this._closeButton.addEventListener(MouseEvent.CLICK, this.hidePanel);
			this._switchButton.addEventListener(MouseEvent.CLICK, this.switchUser);
			this._usernameField.addEventListener(Event.CHANGE, this.handleTextChanged);
			
			this._usernameField.text = "";
			this._switchButton.enabled = false;
			this._switchButton.alpha =  0.5;
			
			this._dialogClip.visible = true;
			this._dialogClip.stage.focus = _usernameField;
		}

		private function handleTextChanged(event : Event) : void 
		{
			this._switchButton.enabled = (_usernameField.text != "");
			this._switchButton.alpha = (_usernameField.text != "" ? 1 : 0.5);
		}

		private function hidePanel(event : MouseEvent) : void 
		{
			hide();
		}

		private function switchUser(event : MouseEvent) : void 
		{
	//		AppState.getInstance().reset();
			AppState.getInstance().user.username = _usernameField.text;
			AppController.getInstance().setLoggedIn(true);
			AppController.getInstance().load();
			this.hide();
		}

		public function hide() : void
		{
			
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_HIDDEN, true));
			this._closeButton.removeEventListener(MouseEvent.CLICK, this.hidePanel);
			this._switchButton.removeEventListener(MouseEvent.CLICK, this.switchUser);
			this._dialogClip.visible = false;
		}
	}
}
