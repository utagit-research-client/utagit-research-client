package it.utag.controller.dialog 
{
	import it.utag.controller.AppController;
	import it.utag.controller.Controller;
	import it.utag.controller.event.UIEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.gfxImportDialog;
	import it.utag.model.AppState;

	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author mikej
	 */
	public class ImportController extends Controller 
	{

		
		private const youTubeMatch : RegExp = /^(.+)youtube\.com(.+)v[\/=](.+)$/;

		private var _dialogClip : gfxImportDialog;
		private var _idField : TextField;
		private var _importButton : SimpleButton;
		private var _closeButton : MovieClip;

		public function ImportController(dialogPanel : MovieClip)
		{
			this._dialogClip = new gfxImportDialog();
			dialogPanel.addChild(this._dialogClip);
			this._dialogClip.visible = false;
			this._dialogClip.x = (dialogPanel.stage.width - _dialogClip.width) / 2;
			this._dialogClip.y = (dialogPanel.stage.height - _dialogClip.height) / 2;
			
			this._idField = this._dialogClip.txtYoutubeID;
			this._importButton = this._dialogClip.btnImport;
			this._closeButton = this._dialogClip.btnClose;
			this._closeButton.buttonMode = true;
			
			super();
		}

		public override function init() : void
		{
			this.dispatchEvent(new Event(Event.INIT));
		}

		public function show() : void 
		{
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_SHOWN, true));
			this._closeButton.addEventListener(MouseEvent.CLICK, this.hidePanel);
			this._idField.addEventListener(Event.CHANGE, this.handleTextChanged);
			
			this._idField.text = "";
			this._importButton.enabled = false;
			this._importButton.useHandCursor = false;
			this._importButton.alpha = 0.5;
			
			this._dialogClip.visible = true;
			this._dialogClip.stage.focus = _idField;
		}

		private function handleTextChanged(event : Event) : void 
		{
			var validURL : Boolean = false;
			
			if(_idField.text.match(youTubeMatch) != null)
				validURL = true;
			
			this._importButton.enabled = validURL;
			this._importButton.alpha = (validURL ? 1 : 0.5);
			this._importButton.useHandCursor = validURL;
			
			if(validURL)
			{
				if(!this._importButton.hasEventListener(MouseEvent.CLICK))
				{
					this._importButton.addEventListener(MouseEvent.CLICK, this.importVideo);
				}
			}
			else
			{
				this._importButton.removeEventListener(MouseEvent.CLICK, this.importVideo);
			}
		}

		private function hidePanel(event : MouseEvent) : void 
		{
			hide();
		}

		private function importVideo(event : MouseEvent) : void 
		{
			AppState.getInstance().reset();
			
			var matches : Array = _idField.text.match(youTubeMatch);
			
			AppState.getInstance().media.url = "http://www.youtube.com/v/" + matches[3];
			AppController.getInstance().load();
			this.hide();
		}

		public function hide() : void
		{
			
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_HIDDEN, true));
			this._closeButton.removeEventListener(MouseEvent.CLICK, this.hidePanel);
			this._importButton.removeEventListener(MouseEvent.CLICK, this.importVideo);
			this._dialogClip.visible = false;
		}
	}
}
