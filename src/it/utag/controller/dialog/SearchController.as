package it.utag.controller.dialog 
{
	import it.utag.controller.AppController;
	import it.utag.controller.Controller;
	import it.utag.controller.event.UIEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.controller.manager.ServiceManager;
	import it.utag.gfxMediaDatum;
	import it.utag.gfxSearchDialog;
	import it.utag.model.AppState;
	import it.utag.model.Datum;
	import it.utag.model.Media;
	import it.utag.service.ServiceEvent;
	import it.utag.service.impl.MediaMetadataService;
	import it.utag.service.impl.SearchMediaService;
	import it.utag.view.AutoCompleteField;
	import it.utag.view.slider.Slider;
	import it.utag.view.slider.SliderEvent;

	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.utils.Timer;

	/**
	 * @author moj
	 */
	public class SearchController extends Controller 
	{
		private var _searchField : TextField;
		private var _searchMediaService : SearchMediaService;
		private var _mediaMetadataService : MediaMetadataService;

		private var _searchDialog : gfxSearchDialog;
		private var _parent : MovieClip;
		private var _closeButton : MovieClip;
		private var _searchButton : SimpleButton;
		private var _videoClips : Array;
		private var _mask : Sprite;
		private var _scroller : MovieClip;
		private var _track : MovieClip;
		private var _upArrow : MovieClip;
		private var _downArrow : MovieClip;
		private var _scrollbar : Slider;
		private var _scrollTimer : Timer;
		private var _scrollDirection : int;
		private var _clipData : Dictionary;
		private var _data : Array;
		private var _autoClip : MovieClip;
		private var _autoField : AutoCompleteField;

		public function SearchController(parent : MovieClip, guide : MovieClip)
		{
			super();
			this._parent = parent;
			this._searchDialog = new gfxSearchDialog();	
			this._closeButton = _searchDialog.btnClose;
			this._searchButton = _searchDialog.btnSearch;
			
			
			this._autoClip = _searchDialog.autoClip as MovieClip;
			this._autoField = new AutoCompleteField(_autoClip["tagName"],_autoClip["completeGuide"]);
			
			this._searchField = _autoClip["tagName"];
			
			this._scroller = _searchDialog["sb"]["scroller"] as MovieClip;
			this._track = _searchDialog["sb"]["track"] as MovieClip;
			this._upArrow = _searchDialog["sb"]["upArrow"] as MovieClip;
			this._downArrow = _searchDialog["sb"]["downArrow"] as MovieClip;
			this._scrollbar = new Slider(this._track, this._scroller, Slider.DIR_VERTICAL, 3, 6);
			
			this._searchMediaService = new SearchMediaService();
			this._mediaMetadataService = new MediaMetadataService();
			this._parent.addChild(_searchDialog);
			_searchDialog.x = guide.x;
			_searchDialog.y = guide.y;
			_searchDialog.visible = false;
			
			this._mask = new Sprite();
			this._mask.graphics.beginFill(0xFF0000);
			this._mask.graphics.drawRect(0, 0, this._searchDialog.datumMask.width, this._searchDialog.datumMask.height);
			this._mask.graphics.endFill();
			this._searchDialog.addChild(this._mask);
			this._mask.y = this._searchDialog.guideDatum.y;
			this._mask.x = this._searchDialog.guideDatum.x;
			
			this._searchDialog.guideDatum.mask = _mask;
			
			this._upArrow.buttonMode = true;
			this._downArrow.buttonMode = true;
			this._closeButton.buttonMode = true;
			
		}

		public override function init() : void
		{
			this._scrollbar.addEventListener(SliderEvent.MOVED, this.handleSliderMoved);
			this._upArrow.addEventListener(MouseEvent.MOUSE_DOWN, this.handleScrollUpClick);
			this._downArrow.addEventListener(MouseEvent.MOUSE_DOWN, this.handleScrollDownClick);
			
			ServiceManager.getInstance().addEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleServiceComplete);
			this.dispatchEvent(new Event(Event.INIT));
		}

		private function handleScrollUpClick(event : MouseEvent) : void 
		{
			startScrolling(-1);
		}

		private function handleScrollDownClick(event : MouseEvent) : void 
		{
			startScrolling(1);
		}

		private function startScrolling(scrollDirection : int) : void
		{
			this._scrollDirection = scrollDirection;
			this._searchDialog.stage.addEventListener(MouseEvent.MOUSE_UP, this.handleScrollEnd);
			this._scrollTimer = new Timer(100);
			this._scrollTimer.addEventListener(TimerEvent.TIMER, this.handleScrollTimer);
			this._scrollTimer.start();
			handleScrollTimer(null);
		}

		private function handleScrollEnd(event : MouseEvent) : void 
		{
			this._scrollTimer.removeEventListener(TimerEvent.TIMER, this.handleScrollTimer);
			this._scrollTimer.stop();
		}

		private function handleScrollTimer(event : TimerEvent) : void 
		{
			// Height of one tag as a ratio of the whole height
			var scrollIncrement : Number = (this._searchDialog.guideDatum.height / this._videoClips.length) / (this._searchDialog.guideDatum.height - this._mask.height);
			this._scrollbar.position = (this._scrollbar.position + (_scrollDirection * scrollIncrement));
		}

		private function handleSliderMoved(event : SliderEvent) : void 
		{
			// At 0, the top of the list is visible. At 100, the whole bottom of the list is visible.
			// Y can therefore go from 0 to (container.height-mask.height)
			var newPos : Number = (this._searchDialog.guideDatum.height - this._mask.height) * this._scrollbar.position;
			this._searchDialog.guideDatum.y = -newPos + this._mask.y;
		}

		private function handleServiceComplete(event : ServiceEvent) : void
		{
			if(event.service == _searchMediaService)
			{
				this._data = _searchMediaService.response.data as Array;
				// Fetch the metadata for each datum
				if(this._data.length > 0)
				{
					this.renderNextDatum();	
				}
				else
				{
					updateScrollBar();
				}
			}
			else if(event.service == _mediaMetadataService)
			{
				var media : Media = (_mediaMetadataService.response.data as Media);
				
				var newTagClip : gfxMediaDatum = new it.utag.gfxMediaDatum();
				newTagClip.tname.text = media.title;
				this._searchDialog.guideDatum.addChild(newTagClip);
				newTagClip.y = this._videoClips.length * newTagClip.height;
					
				var imageLoader : Loader = new Loader();
				imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, this.handleComplete);
				imageLoader.load(new URLRequest(media.thumbURL));
				newTagClip.guideThumb.addChild(imageLoader);
				
				var hitClip : MovieClip = new MovieClip();
				hitClip.graphics.beginFill(0, 0);
				hitClip.graphics.drawRect(0, 0, newTagClip.width, newTagClip.height);
				hitClip.graphics.endFill();
				hitClip.buttonMode = true;
				hitClip.addEventListener(MouseEvent.CLICK, this.handleDatumClick);
				newTagClip.addChild(hitClip);
					
				this._videoClips.push(newTagClip);
				this._clipData[hitClip] = media;
				
				if(_data.length > 0)
				{
					renderNextDatum();
				}
				else
				{
					updateScrollBar();
				}
			}
		}

		private function handleComplete(event : Event) : void
		{				
			var loader : Loader = (event.target as LoaderInfo).loader;
			loader.scaleX = 120 / loader.width;
			loader.scaleY = 90 / loader.height;
		}

		private function updateScrollBar() : void
		{
			// Check to see if we need a scrollbar
			if(this._searchDialog.guideDatum.height > this._mask.height)
			{
				this._scroller.visible = true;
			}
			else
			{
				this._scroller.visible = false;
			}
		}

		private function renderNextDatum() : void
		{		
			var datum : Datum = this._data.pop();
			_mediaMetadataService.id = datum.hidden;
			ServiceManager.getInstance().sendRequest(_mediaMetadataService);
		}

		private function handleDatumClick(event : MouseEvent) : void
		{
			var media : Media = (_clipData[event.target] as Media);
	//		AppState.getInstance().reset();
			AppState.getInstance().media = media;
			this.hide();
			AppController.getInstance().load();
		}

		public function show() : void 
		{
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_SHOWN, true));
			this._closeButton.addEventListener(MouseEvent.CLICK, this.hidePanel);
			this._searchButton.addEventListener(MouseEvent.CLICK, this.performSearch);
			this._searchDialog.visible = true;
			//handleSearchClick(null);	
		}

		public function hide() : void
		{	
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_HIDDEN, true));
			this._closeButton.removeEventListener(MouseEvent.CLICK, this.hidePanel);
			this._searchButton.removeEventListener(MouseEvent.CLICK, this.performSearch);
			this._searchDialog.visible = false;
		}

		private function performSearch(event : MouseEvent) : void 
		{
			if(this._videoClips)
			{
				for each(var tag : gfxMediaDatum in this._videoClips)
				{
					this._searchDialog.guideDatum.removeChild(tag);	
				}
			}
			this._videoClips = [];
			this._clipData = new Dictionary();
			
			_searchMediaService.tagName = _searchField.text;
			ServiceManager.getInstance().sendRequest(_searchMediaService);
		}

		private function hidePanel(event : MouseEvent) : void 
		{
			this.hide();
		}
	}
}
