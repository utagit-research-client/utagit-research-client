package it.utag.controller 
{
	import it.utag.controller.event.SnapshotEvent;
	import it.utag.controller.event.TagInstanceEvent;
	import it.utag.controller.event.VertexEvent;
	import it.utag.controller.event.WaveEvent;
	import it.utag.controller.event.WaveTagEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.model.AppState;
	import it.utag.model.ChangeItem;
	import it.utag.model.Config;
	import it.utag.model.Tag;
	import it.utag.model.TagInstance;
	import it.utag.service.impl.taggingevent.AddTagInstanceService;
	import it.utag.view.WaveView;
	import it.utag.view.WavesView;
	import it.utag.view.control.VertexHandle;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;
	import flash.utils.Timer;

	/**
	 * @author mikej
	 */
	public class WavesController extends Controller
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(WavesController);

		private var _wavesView : WavesView;
		private var _sprite : MovieClip;
		private var _selectedWave : Tag;

		// Map from vertexHandle to TagInstance
		private var _vertexMap : Dictionary;

		
		// Collection of all wave views
		private var _waveViews : Array;
		// Map from tag to wave view
		private var _waveViewMap : Dictionary;
		// Map from wave view to tag
		private var _tagMap : Dictionary;
		private var _scrubber : MovieClip;
		private var _addTagInstanceService : AddTagInstanceService;
		private var _updateChangeItem : ChangeItem;

		private var _updateTimer : Timer;
		private var _lastVertexHandle : VertexHandle;
		private var _hasChange : Boolean;

		public function WavesController(sprite : MovieClip, scrubber : MovieClip)
		{
			this._sprite = sprite;
			this._scrubber = scrubber;
			this._scrubber.mouseEnabled = false;
			this._updateTimer = new Timer(1000);
			this._updateTimer.addEventListener(TimerEvent.TIMER, this.handleUpdateTimer);
		
			this._wavesView = new WavesView();
			_sprite.addChild(this._wavesView);
		}

		public override function init() : void
		{
			this._waveViews = [];
			this._waveViewMap = new Dictionary();
			this._vertexMap = new Dictionary();
			this._tagMap = new Dictionary();
			this._selectedWave = null;
			
			this._addTagInstanceService = new AddTagInstanceService();
			
			NotificationManager.getInstance().addEventListener(TagInstanceEvent.TAGINSTANCE_ADDED, this.handleTaggingEventAdded);
			NotificationManager.getInstance().addEventListener(TagInstanceEvent.TAGINSTANCE_REMOVED, this.handleTaggingEventRemoved);
			NotificationManager.getInstance().addEventListener(TagInstanceEvent.TAGINSTANCE_UPDATED, this.handleTaggingEventUpdated);
			NotificationManager.getInstance().addEventListener(WaveTagEvent.WAVETAG_SNAPSHOT_TOGGLE, this.handleWaveTagSnapshotToggle);
			NotificationManager.getInstance().addEventListener(SnapshotEvent.SNAPSHOT_ADDED, this.handleSnapshotAdded);
			
			NotificationManager.getInstance().addEventListener(WaveTagEvent.WAVETAG_SELECTED, this.handleWaveTagSelected);
			NotificationManager.getInstance().addEventListener(WaveEvent.WAVE_REMOVED, this.handleWaveRemoved);
			NotificationManager.getInstance().addEventListener(WaveEvent.WAVE_ADDED, this.handleWaveAdded);
			
			NotificationManager.getInstance().addEventListener(VertexEvent.VERTEX_GRABBED, this.handleVertexGrabbed);
			NotificationManager.getInstance().addEventListener(VertexEvent.VERTEX_MOVED, this.handleVertexMoved);
			NotificationManager.getInstance().addEventListener(VertexEvent.VERTEX_RELEASED, this.handleVertexReleased);
			
			this.dispatchEvent(new Event(Event.INIT));
		}

		public override function start() : void
		{
			var waveTags : Array = AppState.getInstance().waves;
			
			for each(var waveTag : Tag in waveTags)
			{
				this.addWave(waveTag);
			}
			
			if(waveTags.length > 0)
			{
				this.selectWaveForTag(waveTags[0]);
			}
		}

		public override function destroy() : void
		{
			// Clear out any controls that are showing

			if(_wavesView)
			{
				this._wavesView.clearControls();
				for each(var waveView : WaveView in this._waveViews)
				{
					this._wavesView.removeWaveView(waveView);
				}
			}
			
			NotificationManager.getInstance().removeEventListener(TagInstanceEvent.TAGINSTANCE_ADDED, this.handleTaggingEventAdded);
			NotificationManager.getInstance().removeEventListener(TagInstanceEvent.TAGINSTANCE_REMOVED, this.handleTaggingEventRemoved);
			NotificationManager.getInstance().removeEventListener(TagInstanceEvent.TAGINSTANCE_UPDATED, this.handleTaggingEventUpdated);
			
			NotificationManager.getInstance().removeEventListener(WaveTagEvent.WAVETAG_SELECTED, this.handleWaveTagSelected);
			NotificationManager.getInstance().removeEventListener(WaveTagEvent.WAVETAG_SNAPSHOT_TOGGLE, this.handleWaveTagSnapshotToggle);
			NotificationManager.getInstance().removeEventListener(SnapshotEvent.SNAPSHOT_ADDED, this.handleSnapshotAdded);
			
			NotificationManager.getInstance().removeEventListener(WaveEvent.WAVE_REMOVED, this.handleWaveRemoved);
			NotificationManager.getInstance().removeEventListener(WaveEvent.WAVE_ADDED, this.handleWaveAdded);
			
			NotificationManager.getInstance().removeEventListener(VertexEvent.VERTEX_GRABBED, this.handleVertexGrabbed);
			NotificationManager.getInstance().removeEventListener(VertexEvent.VERTEX_MOVED, this.handleVertexMoved);
			NotificationManager.getInstance().removeEventListener(VertexEvent.VERTEX_RELEASED, this.handleVertexReleased);
		}

		private function handleSnapshotAdded(event : SnapshotEvent) : void
		{
			getWaveViewForTag(event.snapshot.tag).toggleSnapshotView(true);
		}

		private function handleWaveTagSnapshotToggle(event : WaveTagEvent) : void
		{
			_logger.debug("Toggle snapshot for " + event.tag);
			getWaveViewForTag(event.tag).toggleSnapshotView();
		}

		private function handleWaveTagSelected(event : WaveTagEvent) : void 
		{
			this.selectWaveForTag(event.tag);
		}

		private function handleWaveAdded(event : WaveEvent) : void 
		{
			this.addWave(event.tag);
			this.selectWaveForTag(event.tag);
		}

		public function selectWaveForTag(tag : Tag, tagInstance : TagInstance = null) : void
		{
			//			AppState.getInstance().clearUndo();
			if(this._selectedWave)
			{
				getWaveViewForTag(_selectedWave).selected = false;
				
				// Enable selection on the current wave.
				var oldWaveView : WaveView = getWaveViewForTag(_selectedWave);
				oldWaveView.tagView.addEventListener(MouseEvent.CLICK, handleWaveViewClicked);
				oldWaveView.tagView.buttonMode = true;
			}
			
			this._selectedWave = tag;
			var selectedWaveView : WaveView = getWaveViewForTag(tag);
			
			this.redrawControls(tag, tagInstance);
			selectedWaveView.selected = true;
			this._wavesView.bringToFront(selectedWaveView);
		
			NotificationManager.getInstance().dispatchEvent(new WaveEvent(WaveEvent.WAVE_SELECTED, _selectedWave));
		}

		private function redrawControls(tag : Tag, selectedTagInstance : TagInstance = null) : void
		{
			_logger.debug("Redraw controls");
			// Clear the current controls
			this._wavesView.clearControls();
			this._vertexMap = new Dictionary();
			
			var instances : Array = AppState.getInstance().getTagInstancesForTag(tag);
			var matched : Boolean = false;	
			// Add new handles
			for each(var instance : TagInstance in instances)
			{
				
				var vertexHandle : VertexHandle = _wavesView.addControl(instance);
				this._vertexMap[vertexHandle] = instance;
				//		_logger.debug("Compare to "+instance);

				if(selectedTagInstance && selectedTagInstance.instant == instance.instant && selectedTagInstance.relevance == instance.relevance)
				{
					_logger.debug("Match! " + instance);
					vertexHandle.selected = true;
					matched = true;
					_wavesView.selectedHandle = vertexHandle;
				}
			}
			
			_logger.debug("Matched? " + matched);
		}

		private function selectWave(waveView : WaveView, tagInstance : TagInstance = null) : void
		{
			var tag : Tag = _tagMap[waveView];
			this.selectWaveForTag(tag, tagInstance);
		}

		public function addWave(tag : Tag) : void
		{
			var waveView : WaveView = new WaveView(tag, Config.WAVE_COLOURS[_waveViews.length]);
			
			this._wavesView.addWaveView(waveView);
			this._waveViewMap[tag] = waveView;
			this._tagMap[waveView] = tag;
			this._waveViews.push(waveView);
			
			waveView.redrawTag();
			waveView.redrawSnapshot();
			waveView.toggleSnapshotView(true);
			waveView.tagView.addEventListener(MouseEvent.CLICK, handleWaveViewClicked);
			waveView.tagView.buttonMode = true;
		}

		
		private function handleWaveViewClicked(event : MouseEvent) : void 
		{
			if(event.target.parent != getWaveViewForTag(_selectedWave))
			{
				this.selectWave(event.target.parent as WaveView);
			}
			else
			{
				
				var duration : Number = AppState.getInstance().media.duration;
				var instant : Number = Math.round((event.localX / (_wavesView.waveContainer.width / duration)) * 1000);
				var relevance : Number = 1 - ((event.localY) / (_wavesView.waveContainer.height - Config.WAVE_PAD));
				
				var tagInstance : TagInstance = new TagInstance();
				tagInstance.instant = instant;
				tagInstance.media = AppState.getInstance().media;
				tagInstance.relevance = relevance;
				tagInstance.tag = _selectedWave;
				
				var changeItem : ChangeItem = new ChangeItem();
				changeItem.newTagInstance = tagInstance.clone();
				changeItem.type = ChangeItem.TYPE_ADD;
				AppState.getInstance().addTagInstance(tagInstance);
				
				AppState.getInstance().addChangeItem(changeItem);
				
				// y = (1-r)*(h-p)
				// y/(h-p) = 1-r
				// r = 1-(y/(h-p))
			}
		}

		
		private function handleVertexGrabbed(event : VertexEvent) : void
		{
			// Save the current tag instance
			var vertexHandle : VertexHandle = event.vertexHandle;
			var tagInstance : TagInstance = this._vertexMap[vertexHandle];
			
			var newChange : Boolean = false;
			
			if(_hasChange)
			{
				this._updateTimer.reset();
				if(this._lastVertexHandle != vertexHandle)
				{
					_logger.debug("** Different vertex - save last.");
					// If we've got a change and we're dragging a new vertex,
					// save the old one.
					saveChange();
					newChange = true;
				}
				else
				{
					// Reuse the old changemap
					newChange = false;
				}
			}
			else
			{
				newChange = true;
			}
			
			if(newChange)
			{
				this._updateChangeItem = new ChangeItem();
				_updateChangeItem.type = ChangeItem.TYPE_UPDATE;
				_updateChangeItem.oldTagInstance = tagInstance.clone();
				_logger.debug("Grabbed - old: " + _updateChangeItem.oldTagInstance);
			}
		}

		private function handleVertexReleased(event : VertexEvent) : void
		{
			var vertexHandle : VertexHandle = event.vertexHandle;
			var tagInstance : TagInstance = this._vertexMap[vertexHandle];
			
			
			this._lastVertexHandle = vertexHandle;
			// Push the change, and start the save timer.
			_updateChangeItem.newTagInstance = tagInstance.clone();
			
			if(tagInstance.type == TagInstance.TYPE_DELETED)
			{
				
				// Remove the modified vertex
				// TODO : Check this

				AppState.getInstance().removeTagInstance(_updateChangeItem.newTagInstance);
				AppState.getInstance().removeTagInstance(_updateChangeItem.oldTagInstance);
						
				// Deletes should be pushed on immediately.
				_updateChangeItem.type = ChangeItem.TYPE_DELETE;
				_hasChange = true;
				saveChange();
			}
			else if(tagInstance.type == TagInstance.TYPE_DEFAULT)
			{
				_updateChangeItem.type = ChangeItem.TYPE_UPDATE;
				this._hasChange = true;
				this._updateTimer.start();
			}
		}

		public function saveChange() : void
		{
			if(this._hasChange)
			{
				AppState.getInstance().addChangeItem(_updateChangeItem);
				_updateTimer.reset();
			}
			this._hasChange = false;
		}

		// User drags a vertex, and lets go
		// Timer begins (1s)
		// Changes are only saved if:
		// * Timer reaches 1s
		// * User drags a different vertex
		// If user drags the same vertex, the timer is reset.

		private function handleUpdateTimer(event : TimerEvent) : void
		{
			_updateTimer.reset();
			saveChange();
		}

		
		private function handleVertexMoved(event : VertexEvent) : void 
		{
			var waveView : WaveView = getWaveViewForTag(_selectedWave);	
			var vertexHandle : VertexHandle = event.vertexHandle;
			var instance : TagInstance = this._vertexMap[vertexHandle];
			if(vertexHandle.type == TagInstance.TYPE_DEFAULT)
			{
				instance.instant = vertexHandle.getInstant();
				instance.relevance = vertexHandle.getRelevance();
				waveView.redrawTag();		
			}
			instance.type = vertexHandle.type;
		}

		private function handleWaveRemoved(event : WaveEvent) : void
		{
			if(event.tag == _selectedWave)
			{
				this._wavesView.clearControls();
				_selectedWave = null;
			}
			
			var waveView : WaveView = getWaveViewForTag(event.tag);
			this._waveViews.splice(this._waveViews.indexOf(waveView), 1);
			delete this._waveViewMap[event.tag];
			this._wavesView.removeWaveView(waveView);
			
			this.updateColours();
		}

		private function updateColours() : void
		{
			for(var i : int = 0;i < this._waveViews.length;i++)
			{
				(this._waveViews[i] as WaveView).colour = Config.WAVE_COLOURS[i];
			}
		}

		public function getWaveViewForTag(tag : Tag) : WaveView
		{
			return _waveViewMap[tag];
		}

		private function handleTaggingEventUpdated(event : TagInstanceEvent) : void
		{
			_logger.debug("Selected tag instance: " + event.tagInstance);
			var waveView : WaveView = this.getWaveViewForTag(event.tagInstance.tag);
			if(waveView)
			{
				// Add to the wave and redraw

				if(!waveView.selected)
				{
					this.selectWave(waveView, event.tagInstance);
				}
				else
				{
					this.redrawControls(event.tagInstance.tag, event.tagInstance);
				}
				

				waveView.redrawTag();
			}
		}

		private function handleTaggingEventAdded(event : TagInstanceEvent) : void
		{
			var waveView : WaveView = this.getWaveViewForTag(event.tagInstance.tag);
			if(waveView)
			{
				// Add to the wave and redraw

				if(!waveView.selected)
				{
					this.selectWave(waveView, event.tagInstance);
				}
				else
				{
					this.redrawControls(event.tagInstance.tag, event.tagInstance);
				}
				

				waveView.redrawTag();
			}
		}

		private function handleTaggingEventRemoved(event : TagInstanceEvent) : void 
		{
			_logger.debug("Removing tagging event");
			// Check to see if that's the last one.
			var instances : Array = AppState.getInstance().getTagInstancesForTag(event.tagInstance.tag);
			if(instances.length == 0)
			{
				_logger.debug("None left!");
				AppState.getInstance().removeWave(event.tagInstance.tag);
			}
			else
			{
				var waveView : WaveView = this.getWaveViewForTag(event.tagInstance.tag);
				if(waveView)
				{
					_logger.debug("Redraw controls");
					this.redrawControls(event.tagInstance.tag);
					waveView.redrawTag();
				}
			}
		}

		
		public function get wavesView() : WavesView
		{
			return _wavesView;
		}

		public function get hasChange() : Boolean
		{
			return _hasChange;
		}
	}
}
