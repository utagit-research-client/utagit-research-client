package it.utag.controller 
{
	import it.utag.controller.event.SnapshotEvent;
	import it.utag.controller.event.WaveEvent;
	import it.utag.controller.event.WaveTagEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.controller.manager.ServiceManager;
	import it.utag.gfxWaveTag;
	import it.utag.model.AppState;
	import it.utag.model.Config;
	import it.utag.model.Snapshot;
	import it.utag.model.Tag;
	import it.utag.model.TagInstance;
	import it.utag.service.ServiceEvent;
	import it.utag.service.impl.SaveSnapshotService;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;

	/**
	 * @author mikej
	 */
	public class WaveTagsController extends Controller 
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(WaveTagsController);
		private var _tagSlots : Array;
		private var _tags : Array;
		private var _selectedTag : Tag;
		private var _saveSnapshotService : SaveSnapshotService;
		private var _snapshot : Snapshot;
		private var _snapshotState : Dictionary;
		
		private const SNAPSHOT_VISIBLE : String = "snapshotVisible";
		private const SNAPSHOT_INVISIBLE : String = "snapshotInvisible";
		private const SNAPSHOT_UNAVAILABLE : String = "snapshotUnavailable";

		public function WaveTagsController(sprite : Sprite)
		{
			super();

			this._saveSnapshotService = new SaveSnapshotService();
			
			this._tagSlots = [];
			this._snapshotState = new Dictionary();
			var y : uint = 0;
			for(var i : uint = 1;i <= Config.MAX_WAVES;i++)
			{
				var waveTag : gfxWaveTag = new gfxWaveTag();
				waveTag.deleteButton.visible = false;
				waveTag.waveTagColour.visible = false;
				waveTag.snapshotButton.visible = false;
				waveTag.waveTagColour.graphics.beginFill(Config.WAVE_COLOURS[i - 1]);
				waveTag.waveTagColour.graphics.drawCircle(0, 0, 5);
				waveTag.waveTagColour.graphics.endFill();
				sprite.addChild(waveTag);
				waveTag.y = y;
				_tagSlots.push(waveTag);
				y += waveTag.height;
			}
			
			this._tags = [];
		}

		public override function init() : void
		{
			NotificationManager.getInstance().addEventListener(WaveEvent.WAVE_ADDED, this.handleWaveAdded);
			NotificationManager.getInstance().addEventListener(WaveEvent.WAVE_SELECTED, this.handleWaveSelected);
			NotificationManager.getInstance().addEventListener(WaveEvent.WAVE_REMOVED, this.handleWaveRemoved);
			NotificationManager.getInstance().addEventListener(SnapshotEvent.SNAPSHOT_ADDED, this.handleSnapshotAdded);
			this.dispatchEvent(new Event(Event.INIT));
		}

		public override function start() : void
		{
			for each(var tag : Tag in AppState.getInstance().waves)
			{
				_logger.debug("Add wave: " + tag);
				this.addWave(tag);
			}
		}

		public override function destroy() : void
		{
			NotificationManager.getInstance().removeEventListener(WaveEvent.WAVE_ADDED, this.handleWaveAdded);
			NotificationManager.getInstance().removeEventListener(WaveEvent.WAVE_SELECTED, this.handleWaveSelected);
			NotificationManager.getInstance().removeEventListener(WaveEvent.WAVE_REMOVED, this.handleWaveRemoved);
			NotificationManager.getInstance().removeEventListener(SnapshotEvent.SNAPSHOT_ADDED, this.handleSnapshotAdded);
			this._tags = [];
			this._snapshotState = new Dictionary();
			this.redraw();
		}

		private function handleSnapshotAdded(event : SnapshotEvent) : void
		{
			if(_snapshotState[event.snapshot.tag] == SNAPSHOT_UNAVAILABLE)
			{
				_snapshotState[event.snapshot.tag] = SNAPSHOT_VISIBLE;
			}
			this.redraw();
		}

		private function handleWaveRemoved(event : WaveEvent) : void
		{
			this.removeWave(event.tag);
		}

		private function handleWaveSelected(event : WaveEvent) : void 
		{
			this._selectedTag = event.tag;
			this.redraw();
		}

		// Waves are added via the tag view
		private function handleWaveAdded(event : WaveEvent) : void 
		{
			addWave(event.tag);		
		}

		// Waves are removed by clicking their delete button
		private function handleDeleteClick(event : MouseEvent) : void 
		{
			(event.target as MovieClip).removeEventListener(MouseEvent.CLICK, this.handleDeleteClick);
			// Work out which wave is being deleted
			var pos : uint = this._tagSlots.indexOf((event.target as MovieClip).parent);
			removeWave(this._tags[pos]);
		}

		public function addWave(tag : Tag) : void
		{
			if(this._tags.length == Config.MAX_WAVES)
			{
				removeWave(this._tags[0]);
			}
			
			this._tags.push(tag);
			_snapshotState[tag] = SNAPSHOT_UNAVAILABLE;			
			this.redraw();
		}

		private function removeWave(tag : Tag) : void
		{
			var tagPos : uint = this._tags.indexOf(tag);	
			this._tags.splice(tagPos, 1);	
			AppState.getInstance().removeWave(tag);	
			delete _snapshotState[tag];
			this.redraw();
		}

		private function redraw() : void
		{
			for(var i : uint = 0;i < Config.MAX_WAVES;i++)
			{
				var currSlot : gfxWaveTag = (this._tagSlots[i] as gfxWaveTag);
				
				if(i < this._tags.length)
				{
					var tag : Tag = this._tags[i];
					currSlot.tagName.text = tag.object;
					currSlot.tagName.mouseEnabled = false;
					currSlot.snapshotButton.visible = true;
					var snapTags : Array = AppState.getInstance().getSnapshotInstancesForTag(_tags[i]);
					if(snapTags.length > 0)
					{
						switch(_snapshotState[tag])
						{
							case SNAPSHOT_UNAVAILABLE:
								// We're unavailable, but we now have tags. So go to visible.
								_snapshotState[tag] = SNAPSHOT_VISIBLE;
							case SNAPSHOT_VISIBLE:
								currSlot.snapshotButton.alpha = 1;
								break;
							case SNAPSHOT_INVISIBLE:
								currSlot.snapshotButton.alpha = 0.5;
								break;
						}
						
						currSlot.snapshotButton.addEventListener(MouseEvent.CLICK, this.handleSnapshotToggle);
						currSlot.snapshotButton.buttonMode = true;
						currSlot.snapshotButton.visible = true;
					}
					else
					{
						_snapshotState[tag] = SNAPSHOT_UNAVAILABLE;
						currSlot.snapshotButton.alpha = 0.1;
						currSlot.snapshotButton.buttonMode = false;
						currSlot.snapshotButton.visible = true;
					}
					
					currSlot.deleteButton.addEventListener(MouseEvent.CLICK, this.handleDeleteClick);
					currSlot.deleteButton.buttonMode = true;
					currSlot.deleteButton.visible = true;
					
					currSlot.waveTagColour.graphics.lineStyle(2, 0, 0);
					currSlot.waveTagColour.graphics.clear();
					currSlot.waveTagColour.graphics.beginFill(Config.WAVE_COLOURS[i]);
					currSlot.waveTagColour.graphics.drawCircle(0, 0, 5);
					currSlot.waveTagColour.graphics.endFill();
					
					if(this._selectedTag == tag)
					{
						currSlot.waveTagColour.graphics.lineStyle(2, 0, 1);
						currSlot.waveTagColour.graphics.beginFill(0, 0);
						currSlot.waveTagColour.graphics.drawCircle(0, 0, 8);
						currSlot.waveTagColour.graphics.endFill();
						
						currSlot.waveTagColour.removeEventListener(MouseEvent.CLICK, this.handleSelectClick);
						currSlot.waveTagColour.buttonMode = false;
					}
					else
					{
						currSlot.waveTagColour.addEventListener(MouseEvent.CLICK, this.handleSelectClick);
						currSlot.waveTagColour.buttonMode = true;
					}
					
					currSlot.waveTagColour.visible = true;
				}
				else
				{
					_logger.debug("Clearing slot " + i);
					currSlot.tagName.text = "";
					currSlot.snapshotButton.removeEventListener(MouseEvent.CLICK, this.handleSnapshotToggle);
					currSlot.deleteButton.removeEventListener(MouseEvent.CLICK, this.handleDeleteClick);
					currSlot.waveTagColour.removeEventListener(MouseEvent.CLICK, this.handleSelectClick);
					currSlot.waveTagColour.buttonMode = false;
					currSlot.waveTagColour.visible = false;
					currSlot.deleteButton.visible = false;
					currSlot.waveTagColour.visible = false;
					currSlot.snapshotButton.visible = false;
				}
			}
		}

		private function handleSnapshotToggle(event : MouseEvent) : void
		{
			var pos : int = this._tagSlots.indexOf(event.target.parent as MovieClip);
			var tag : Tag = this._tags[pos];	
			
			NotificationManager.getInstance().dispatchEvent(new WaveTagEvent(WaveTagEvent.WAVETAG_SNAPSHOT_TOGGLE, tag));
			if(_snapshotState[tag] == SNAPSHOT_INVISIBLE)
			{
				_snapshotState[tag] = SNAPSHOT_VISIBLE;
			}
			else if(_snapshotState[tag] == SNAPSHOT_VISIBLE)
			{
				_snapshotState[tag] = SNAPSHOT_INVISIBLE;
			}
			
			this.redraw();
		}

		private function handleSelectClick(event : MouseEvent) : void 
		{
			var pos : int = this._tagSlots.indexOf(event.target.parent as MovieClip);
			NotificationManager.getInstance().dispatchEvent(new WaveTagEvent(WaveTagEvent.WAVETAG_SELECTED, this._tags[pos]));
		}

		public function makeSnapshot() : void 
		{
			var instances : Array = AppState.getInstance().getTagInstancesForTag(this._selectedTag);
			
			// Clone 'em all
			var newInstances : Array = [];
			for each(var instance : TagInstance in instances)
			{
				newInstances.push(instance.clone());
			}
			
			_saveSnapshotService.tag = this._selectedTag;
			
			this._snapshot = new Snapshot();
			this._snapshot.creation = new Date();
			this._snapshot.instances = newInstances;
			this._snapshot.tag = _selectedTag;
			
			ServiceManager.getInstance().addEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleSnapshotServiceComplete);
			ServiceManager.getInstance().sendRequest(this._saveSnapshotService);
		}

		private function handleSnapshotServiceComplete(event : ServiceEvent) : void
		{
			if(event.service == this._saveSnapshotService)
			{
				// We should call the snapshot service here.
				AppState.getInstance().user.addSnapshot(this._snapshot);
				_snapshotState[this._snapshot.tag] = SNAPSHOT_VISIBLE;
				this.redraw();
			}
		}
	}
}
