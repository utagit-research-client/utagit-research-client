package it.utag.controller 
{
	import it.utag.view.AutoCompleteField;
	import it.utag.controller.event.UIEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.controller.manager.ServiceManager;
	import it.utag.model.AppState;
	import it.utag.model.ChangeItem;
	import it.utag.model.Tag;
	import it.utag.model.TagInstance;
	import it.utag.service.ServiceEvent;
	import it.utag.service.impl.taggingevent.AddTagInstanceService;
	import it.utag.view.slider.Slider;
	import it.utag.view.slider.SliderEvent;

	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author moj
	 */
	public class AddTagController extends Controller
	{
		private var _tagPanel : Sprite;
		private var _tagNameField : AutoCompleteField;
		private var _tagPredField : TextField;
		private var _relevanceField : TextField;
		private var _relevanceSlider : Slider;
		private var _closeButton : MovieClip;
		private var _saveButton : SimpleButton;

		private var _addTagInstanceService : AddTagInstanceService;
		private var _addedInstance : TagInstance;
		private var _tagNameTextField : TextField;
		private var _tagNameClip : MovieClip;

		public function AddTagController(sprite : Sprite)
		{
			this._tagPanel = sprite;

			this._tagNameClip = this._tagPanel["tagName"] as MovieClip;
			this._tagNameTextField = this._tagNameClip["tagName"] as TextField;

			this._tagNameField = new AutoCompleteField(_tagNameClip["tagName"],_tagNameClip["completeGuide"]);
			
			this._tagPredField = this._tagPanel["tagPredicate"] as TextField;
			this._tagPredField.text = "is_a";
			this._tagPredField.selectable = false;
			this._relevanceField = this._tagPanel["relevanceTxt"] as TextField;
			this._relevanceSlider = new Slider(this._tagPanel["relevanceSlider"]["sliderBar"], this._tagPanel["relevanceSlider"]["slider"]);
			this._relevanceSlider.position = 0.5;
			updateRelevanceField();
			
			this._closeButton = this._tagPanel["closeTagPanelBtn"];
			this._closeButton.buttonMode = true;
			
			this._saveButton = this._tagPanel["saveButton"];
		
			this._addTagInstanceService = new AddTagInstanceService();
			
			this.hide();
		}

		private function updateRelevanceField() : void 
		{
			this._relevanceField.text = "" + _relevanceSlider.position.toFixed(4);
		}

		public override function init() : void
		{
			this._relevanceSlider.addEventListener(SliderEvent.MOVED, this.handleSliderMoved);
			this.dispatchEvent(new Event(Event.INIT));
		}
		
		public override function destroy() : void
		{
			this._relevanceSlider.removeEventListener(SliderEvent.MOVED, this.handleSliderMoved);
		}

		private function handleSliderMoved(event : SliderEvent) : void 
		{
			updateRelevanceField();
		}

		public function togglePanel() : void
		{
			if(this._tagPanel.visible)
			{
				this.hide();
			}
			else
			{
				this.show();
			}
		}

		private function hide() : void
		{
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_HIDDEN));
			this._closeButton.removeEventListener(MouseEvent.CLICK, this.hidePanel);
			this._saveButton.removeEventListener(MouseEvent.CLICK, this.saveTag);
			this._tagPanel.visible = false;
		}

		private function show() : void
		{	
			NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.OVERLAY_SHOWN));
			this._closeButton.addEventListener(MouseEvent.CLICK, this.hidePanel);
			this._saveButton.addEventListener(MouseEvent.CLICK, this.saveTag);
			this._tagPanel.visible = true;
			this._tagPanel.stage.focus = this._tagNameField;
		}

		private function hidePanel(event : MouseEvent) : void
		{
			hide();
		}

		private function saveTag(event : MouseEvent) : void
		{
			// Check to see if tag already exists
			var tag : Tag;
			if(!AppState.getInstance().haveTag(this._tagNameTextField.text))
			{
				tag = new Tag();
				tag.object = this._tagNameTextField.text;
				tag.predicate = this._tagPredField.text;
				tag.added_on = new Date();
				tag.modified_on = new Date();
				AppState.getInstance().addTag(tag);
			}
			else
			{
				tag = AppState.getInstance().getTag(this._tagNameTextField.text);
			}
			
			var tagInstance : TagInstance = new TagInstance();
			tagInstance.instant = Math.round(AppState.getInstance().media.position * 1000);
			tagInstance.media = AppState.getInstance().media;
			tagInstance.relevance = this._relevanceSlider.position;
			tagInstance.creator = AppState.getInstance().user.username;
			tagInstance.tag = tag;
			
			
			
			var changeItem : ChangeItem = new ChangeItem();
			changeItem.newTagInstance = tagInstance.clone();
			changeItem.type = ChangeItem.TYPE_ADD;
		
			AppState.getInstance().addChangeItem(changeItem);
			
			// Add to the app state
			if(!AppState.getInstance().haveTag(tagInstance.tag.object))
			{
				AppState.getInstance().addTag(tagInstance.tag, true);
			}
			AppState.getInstance().addTagInstance(tagInstance);
			AppState.getInstance().addWave(tagInstance.tag);
			this.hide();
			
	//		this._addedInstance = tagInstance;
			
			
	//		ServiceManager.getInstance().addEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleAddedInstance);
	//		_addTagInstanceService.tagInstance = tagInstance;
	//		ServiceManager.getInstance().sendRequest(_addTagInstanceService);
		}

		private function handleAddedInstance(event : ServiceEvent) : void 
		{
			if(event.service == this._addTagInstanceService)
			{
				ServiceManager.getInstance().removeEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleAddedInstance);
			
				// Add to the app state
				if(!AppState.getInstance().haveTag(_addedInstance.tag.object))
				{
					AppState.getInstance().addTag(_addedInstance.tag, true);
				}
				AppState.getInstance().addWave(_addedInstance.tag);
				AppState.getInstance().addTagInstance(_addedInstance);
				this.hide();
			}
		}
	}
}
