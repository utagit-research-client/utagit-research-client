package it.utag.controller 
{
	import it.utag.controller.event.UIEvent;
	import it.utag.controller.event.VertexEvent;
	import it.utag.controller.event.VideoEvent;
	import it.utag.controller.event.WaveTagEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.model.AppState;
	import it.utag.model.Config;
	import it.utag.model.Tag;
	import it.utag.model.TagInstance;
	import it.utag.view.CloudTextField;

	import com.gskinner.motion.GTween;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import flash.utils.Timer;

	/**
	 * @author moj
	 */
	public class CloudController extends Controller 
	{
		private var _container : MovieClip;
		private var _cloudClip : MovieClip;

		private var _tagSlots : Array;

		// Map from tag to TextField
		private var _tagSlotMap : Dictionary;
		// Map from TextField to GTween
		private var _slotTweenMap : Dictionary;
		// Map from TextField to tag
		private var _slotTagMap : Dictionary;
		// List of tags
		private var _tags : Array;
		private var _format : TextFormat;

		private var _dragging : Boolean;
		private var _downTimer : Timer;
		private var _mouseDown : Boolean;
		private var _seekbar : MovieClip;

		public function CloudController(container : MovieClip, seekbar : MovieClip)
		{
			this._container = container;	
			
			// Set up the cloud layer (!)
			this._cloudClip = new MovieClip();
			this._cloudClip.graphics.lineStyle(1, 0, 0);
			this._cloudClip.graphics.beginFill(0xff0000, 0);
			this._cloudClip.graphics.drawRect(0, 0, Config.VIDEO_WIDTH, Config.VIDEO_HEIGHT);
			this._cloudClip.graphics.endFill();
			this._cloudClip.mouseChildren = true;
			this._seekbar = seekbar;
			
			this._downTimer = new Timer(250);
			this._downTimer.addEventListener(TimerEvent.TIMER, this.handleDownTimer);
			
			this._container.addChild(this._cloudClip);
		}

		public override function init() : void
		{
			this._tagSlots = [];
			this._tagSlotMap = new Dictionary();
			this._slotTagMap = new Dictionary();
			this._slotTweenMap = new Dictionary();
			this._tags = [];
			this._dragging = false;
			
			this.generateSlots();
			
			NotificationManager.getInstance().addEventListener(VideoEvent.PAUSED, this.handlePaused);
			NotificationManager.getInstance().addEventListener(VideoEvent.RESUMED, this.handleResumed);
			
			this._cloudClip.addEventListener(MouseEvent.MOUSE_DOWN, this.handleMouseDown);
			this._cloudClip.addEventListener(MouseEvent.MOUSE_UP, this.handleMouseUp);
			
			this.dispatchEvent(new Event(Event.INIT));
		}

		public override function destroy() : void 
		{
			if(this._tagSlots != null && this._tagSlots.length > 0)
			{
				for(var i : uint = 0;i < this._tagSlots.length; i++)
				{
					var clip : CloudTextField = (_tagSlots[i] as CloudTextField);
					
					clip.parent.removeChild(clip);
				}
			}
			
			NotificationManager.getInstance().removeEventListener(VideoEvent.PAUSED, this.handlePaused);
			NotificationManager.getInstance().removeEventListener(VideoEvent.RESUMED, this.handleResumed);
			
			this._cloudClip.removeEventListener(MouseEvent.MOUSE_DOWN, this.handleMouseDown);
			this._cloudClip.removeEventListener(MouseEvent.MOUSE_UP, this.handleMouseUp);
		}

		private function generateSlots() : void
		{
			for(var y : uint = 0;y < 3;y++)
			{
				for(var x : uint = 0;x < 3;x++)
				{
					// Work out the x divide in pixels
					var xDiv : Number = Config.VIDEO_WIDTH * (x / 3);
					var yDiv : Number = Config.VIDEO_HEIGHT * (y / 3);
					
					var field : CloudTextField = new CloudTextField();
					this._cloudClip.addChild(field);
					field.x = xDiv + (Config.VIDEO_WIDTH / 6);
					field.y = yDiv;
					field.addEventListener(MouseEvent.CLICK, this.handleFieldClick);
					field.buttonMode = true;

					this._tagSlots[(y * 3) + x] = field;
					this._slotTweenMap[field] = new GTween();
				}
			}
		}

		private function handleFieldClick(event : MouseEvent) : void
		{
			var field : CloudTextField = (event.target as CloudTextField);
			if(_slotTagMap[field])
			{
				var tag : Tag = _slotTagMap[field] as Tag;
				if(!AppState.getInstance().haveWave(tag))
				{
					AppState.getInstance().addWave(AppState.getInstance().getTag(tag.object));
				}
				else
				{
					
					NotificationManager.getInstance().dispatchEvent(new WaveTagEvent(WaveTagEvent.WAVETAG_SELECTED, tag));
				}
			}
		}
		
		public function toggleCloud() : void
		{
			this._cloudClip.visible = !this._cloudClip.visible;
		}


		private function handleMouseUp(event : MouseEvent) : void
		{
			this._mouseDown = false;
			if(!this._dragging)
			{
			}
			else
			{
				this._cloudClip.removeEventListener(MouseEvent.MOUSE_MOVE, this.handleMouseMove);
				this._dragging = false;
				NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.TAGCLOUD_DRAGEND));
			}
		}

		private function handleMouseDown(event : MouseEvent) : void
		{
			this._mouseDown = true;
			this._dragging = false;
			
			// Start a short timer.
			this._downTimer.reset();
			this._downTimer.start();
		}

		private function handleDownTimer(event : TimerEvent) : void
		{
			this._downTimer.stop();
			
			// If we haven't released the mouse button, we're now dragging.
			if(this._mouseDown)
			{
				this._dragging = true;
				NotificationManager.getInstance().dispatchEvent(new UIEvent(UIEvent.TAGCLOUD_DRAGSTART));
				this._cloudClip.addEventListener(MouseEvent.MOUSE_MOVE, this.handleMouseMove);
			}
		}

		private function handleMouseMove(event : MouseEvent) : void
		{
			var mouseX : Number = _seekbar.globalToLocal(new Point(event.stageX, event.stageY)).x;
			
			var newPos : Number = (mouseX / _seekbar["progress_mc"].width) * AppState.getInstance().media.duration;;
			if(newPos < 0) newPos = 0;
			if(newPos > AppState.getInstance().media.duration) newPos = AppState.getInstance().media.duration;
			AppState.getInstance().media.position = newPos;
			this.updateCloud();
		}

		
		private function handleResumed(event : VideoEvent) : void
		{
			NotificationManager.getInstance().addEventListener(VideoEvent.POSITION_CHANGED, this.handlePositionChanged);
//			NotificationManager.getInstance().removeEventListener(VertexEvent.VERTEX_MOVED, this.handleVertexMoved);
		}

		private function handlePaused(event : VideoEvent) : void
		{
			//			NotificationManager.getInstance().addEventListener(VertexEvent.VERTEX_MOVED, this.handleVertexMoved);
			NotificationManager.getInstance().removeEventListener(VideoEvent.POSITION_CHANGED, this.handlePositionChanged);
		}

		private function handleVertexMoved(event : VertexEvent) : void
		{
			updateCloud();
		}

		private function handlePositionChanged(event : VideoEvent) : void
		{
			updateCloud();
		}

		private function updateCloud() : void
		{
			var pos : uint = AppState.getInstance().media.position * 1000;
			var tagInstances : Array = AppState.getInstance().getTagInstancesAt(pos);
			if(tagInstances.length == 0) return;
			tagInstances.sortOn("relevance");
			
			var newTags : Array = [];
			// Prune out any irrelevant tags
			
			for(var i : uint = 0;i < tagInstances.length;i++)
			{
				var newTag : Tag = (tagInstances[i] as TagInstance).tag;
				newTags.push(newTag);
			}
			
			for each(var currentTag : Tag in this._tags)
			{
				if(newTags.indexOf(currentTag) == -1)
				{
					(_tagSlotMap[currentTag] as CloudTextField).text = "";
					(_tagSlotMap[currentTag] as CloudTextField).scaleX = 0;
					(_tagSlotMap[currentTag] as CloudTextField).scaleY = 0;
					
					delete _slotTagMap[_tagSlotMap[currentTag]];
					delete _tagSlotMap[currentTag];
					this._tags.splice(this._tags.indexOf(currentTag), 1);
				}
			}
			
			for each(var tagInstance : TagInstance in tagInstances)
			{	
				var field : CloudTextField;
				if(!_tagSlotMap[tagInstance.tag])
				{
					// Find next free slot.
					var slot : uint = getNextFreeSlot();
					field = _tagSlots[slot] as CloudTextField;
					_slotTagMap[field] = tagInstance.tag;
					_tagSlotMap[tagInstance.tag] = field;
					_tags.push(tagInstance.tag);
					field.text = tagInstance.tag.object;
					field.setTextFormat(_format);
					field.scaleX = 0;
					field.scaleY = 0;
				}
				else
				{
					field = _tagSlotMap[tagInstance.tag] as CloudTextField;	
				}
				
				_slotTweenMap[field] = new GTween(field, 1, {scaleX:tagInstance.relevance, scaleY:tagInstance.relevance});
			}
		}

		private function getNextFreeSlot() : uint
		{
			// Build up list of availables
			var available : Array = [];
			for(var i : uint = 0;i < _tagSlots.length;i++)
			{
				if(!_slotTagMap[_tagSlots[i]])
				{
					available.push(i);
				}
			}
			
			return available[Math.round(Math.random() * available.length)];
		}
	}
}
