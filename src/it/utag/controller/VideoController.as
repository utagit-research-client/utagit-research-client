package it.utag.controller 
{
	import it.utag.controller.event.UIEvent;
	import it.utag.controller.event.VideoEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.model.AppState;
	import it.utag.model.Config;
	import it.utag.model.Media;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.utils.Timer;

	/**
	 * @author moj
	 */
	public class VideoController extends Controller
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(VideoController);
		private var _sprite : Sprite;
		private var loader : Loader;
		private var player : Object;
		private var _media : Media;
		//	private var _playTimer : Timer;

		// Handle interruption
		private var _resumeVideo : Boolean;

		
		private static const STATE_ENDED : uint = 0;
		private static const STATE_PLAYING : uint = 1;
		private static const STATE_PAUSED : uint = 2;
		//		private static const STATE_BUFFERING : uint = 3;
		//		private static const STATE_CUED : uint = 5;
		private static const PLAYER_URL : String = "http://www.youtube.com/apiplayer?version=3";

		private static const FRAME_PLAY : uint = 1;
		private static const FRAME_PAUSE : uint = 2;

		private var _videoContainer : Sprite;
		private var _playButton : MovieClip;
		private var _muteButton : MovieClip;
		private var _muted : Boolean;
		private var _playTimer : Timer;
		private var _scrubber : MovieClip;
		private var _volumeBar : MovieClip;
		private var _loaded : Boolean;
		private var _previousVolume : uint;
		private var _seekBar : MovieClip;

		
		public function VideoController(sprite : Sprite, scrubber : MovieClip)
		{
			this._scrubber = scrubber;
			this._videoContainer = sprite["videoContainer"] as Sprite;
			this._volumeBar = sprite["volumeBar"] as MovieClip;
			this._playButton = sprite["playbutton_mc"] as MovieClip;
			this._muteButton = sprite["muteButton"] as MovieClip;
			this._seekBar = sprite["seekbar_mc"] as MovieClip;
			this._muteButton.buttonMode = true;
			
			this._sprite = sprite;
			this._media = AppState.getInstance().media;
			this._playTimer = new Timer(500);
			this._playTimer.addEventListener(TimerEvent.TIMER, this.handleTimer);
	
			this._playButton.buttonMode = false;
			this._playButton.alpha = 0.5;
			
			this._muted = false;
			(this._muteButton["on_mc"] as MovieClip).visible = true;
			(this._muteButton["off_mc"] as MovieClip).visible = false;
			this._muteButton.addEventListener(MouseEvent.MOUSE_DOWN, this.handleMuteClicked);
			
			
			this._volumeBar["whiteBarr"].mask = this._volumeBar["volumeMask"];
			this._volumeBar.buttonMode = true;
			this._volumeBar.addEventListener(MouseEvent.MOUSE_DOWN, this.handleVolumeClicked);
			
			//			this._seekBar.buttonMode = true;
			this._seekBar.addEventListener(MouseEvent.MOUSE_DOWN, this.handleSeekClicked);
			
		}
		
		public override function init() : void
		{	
			this.loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			loader.load(new URLRequest(PLAYER_URL));
		}
		
		public override function destroy() : void
		{
			this._playTimer.reset();
			this._loaded = false;
			if(this.player)
			{
				this.player.pauseVideo();
			}
			
			if(this.loader)
			{
				loader.contentLoaderInfo.removeEventListener(Event.INIT, onLoaderInit);
				loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				if(loader.parent)
				{
					_videoContainer.removeChild(loader);
				}
			}
		}

		private function handleVolumeClicked(event : MouseEvent) : void
		{
			this._volumeBar["volumeMask"].width = event.localX;
			this._volumeBar["whiteBarr"].mask = this._volumeBar["volumeMask"];
			this.player.setVolume(event.localX / this._volumeBar.width * 100);
		}

		private function handleSeekClicked(event : MouseEvent) : void 
		{
			var mouseX : Number = _seekBar.globalToLocal(new Point(event.stageX, event.stageY)).x;
			this.player.seekTo((mouseX / _seekBar["progress_mc"].width) * this._media.duration);
		}

		
		private function handleMuteClicked(event : MouseEvent) : void 
		{
			this._muted = !this._muted;
			if(!this._muted)
			{
				(this._muteButton["on_mc"] as MovieClip).visible = true;
				(this._muteButton["off_mc"] as MovieClip).visible = false;
				this.player.setVolume(this._previousVolume);
				this._volumeBar.visible = true;
			}
			else
			{
				(this._muteButton["on_mc"] as MovieClip).visible = false;
				(this._muteButton["off_mc"] as MovieClip).visible = true;
				this._previousVolume = this.player.getVolume();
				this.player.setVolume(0);
				this._volumeBar.visible = false;
			}
		}

		private function onIOError(event : IOErrorEvent) : void 
		{
			_logger.fatal("Error on load: " + event.text);
		}

		private function onLoaderInit(event : Event) : void 
		{
			_videoContainer.addChild(loader);
			this.loader.content.addEventListener("onReady", this.handlePlayerReady);
			this.loader.content.addEventListener("onStateChange", onPlayerStateChange);
		}

		private function handlePlayerReady(event : Event) : void
		{
			this.player = loader.content;
			this.player.setSize(Config.VIDEO_WIDTH, Config.VIDEO_HEIGHT);
			this.player.setVolume(0);
		
			NotificationManager.getInstance().addEventListener(UIEvent.OVERLAY_HIDDEN, this.handleOverlayHidden);
			NotificationManager.getInstance().addEventListener(UIEvent.OVERLAY_SHOWN, this.handleOverlayShown);
			NotificationManager.getInstance().addEventListener(UIEvent.TAGCLOUD_DRAGSTART, this.handleCloudDragStart);
			NotificationManager.getInstance().addEventListener(UIEvent.TAGCLOUD_DRAGEND, this.handleCloudDragEnd);
			this.dispatchEvent(new Event(Event.INIT));
		}

		private function handleCloudDragStart(event : UIEvent) : void
		{
			this.player.pauseVideo();
		}

		private function handleCloudDragEnd(event : UIEvent) : void
		{
			this.player.seekTo(AppState.getInstance().media.position);
			this.player.playVideo();
		}


		public function loadVideo(url : String) : void
		{
			this._loaded = false;
			this._playTimer.reset();
			_logger.info("Load video " + url);
			this.player.loadVideoByUrl(url);
		}

		private function handleOverlayShown(event : UIEvent) : void 
		{
			if(!event.modal)
			{
				return;
			}	
			if(this.player.getPlayerState() == 1)
			{
				this.player.pauseVideo();
				this._resumeVideo = true;
			}
		}

		private function handleOverlayHidden(event : UIEvent) : void 
		{
			if(!event.modal)
				return;
				
			if(this._resumeVideo)
			{
				this.player.playVideo();
			}
			this._resumeVideo = false;
		}

		
		
		public function onPlayerStateChange(event : Event) : void 
		{
			var state : Number = (event as Object).data;
			this._media.duration = this.player.getDuration();
			this.changeButtonState(state);
			if(state == STATE_PLAYING)
			{
				if(!this._loaded)
				{
					_logger.debug("Video is loaded: Set up flags and report back");
					this._media = AppState.getInstance().media;
					this._media.duration = this.player.getDuration();
					this._loaded = true;
					NotificationManager.getInstance().dispatchEvent(new VideoEvent(VideoEvent.LOADED));
				}
				// Start the timer!
				//			this._sprite.stage.addEventListener(Event.ENTER_FRAME, this.handleEnterFrame);
				this._playTimer.start();
				NotificationManager.getInstance().dispatchEvent(new VideoEvent(VideoEvent.RESUMED));
			}
			else
			{
				NotificationManager.getInstance().dispatchEvent(new VideoEvent(VideoEvent.PAUSED));
				this._playButton.gotoAndStop(1);
				//			this._sprite.stage.removeEventListener(Event.ENTER_FRAME, this.handleEnterFrame);
				if(this._playTimer.running)
				{
					this._playTimer.stop();
				}
			}
			
			this._media.position = this.player.getCurrentTime();
		}

		//		private function handleEnterFrame(event : Event) : void 
		//		{
		//			this._media.position = this.player.getCurrentTime();
		//			// Update seek bar
		//			this._sprite["seekbar_mc"]["seekBarWhite"].width = this._sprite["seekbar_mc"]["progress_mc"].width * (this._media.position/this._media.duration);
		//			
		//			NotificationManager.getInstance().dispatchEvent(new VideoEvent(VideoEvent.POSITION_CHANGED, this.player.getCurrentTime()));
		//		}

		private function changeButtonState(state : uint) : void 
		{
			if(state == STATE_PLAYING)
			{
				this._playButton.gotoAndStop(FRAME_PAUSE);
				this._playButton.visible = true;
				this._playButton.removeEventListener(MouseEvent.MOUSE_DOWN, this.handlePlayClicked);
				this._playButton.addEventListener(MouseEvent.MOUSE_DOWN, this.handlePauseClicked);
				this._playButton.buttonMode = true;
			}
			else if(state == STATE_PAUSED || state == STATE_ENDED)
			{
				this._playButton.gotoAndStop(FRAME_PLAY);
				this._playButton.visible = true;
				this._playButton.removeEventListener(MouseEvent.MOUSE_DOWN, this.handlePauseClicked);
				this._playButton.addEventListener(MouseEvent.MOUSE_DOWN, this.handlePlayClicked);
				this._playButton.buttonMode = true;
			}
			else
			{
				this._playButton.buttonMode = false;
				this._playButton.visible = false;
			}
		}

		private function handlePlayClicked(event : MouseEvent) : void 
		{
			this._playButton.removeEventListener(MouseEvent.MOUSE_DOWN, this.handlePlayClicked);
			this.player.playVideo();
		}

		private function handlePauseClicked(event : MouseEvent) : void 
		{
			this._playButton.removeEventListener(MouseEvent.MOUSE_DOWN, this.handlePauseClicked);
			this.player.pauseVideo();
		}

		private function handleTimer(event : TimerEvent) : void 
		{
			this._media.position = this.player.getCurrentTime();
			// Update seek bar
			this._sprite["seekbar_mc"]["seekBarWhite"].width = this._sprite["seekbar_mc"]["progress_mc"].width * (this._media.position / this._media.duration);
			
			NotificationManager.getInstance().dispatchEvent(new VideoEvent(VideoEvent.POSITION_CHANGED, this.player.getCurrentTime()));
		}
	}
}
