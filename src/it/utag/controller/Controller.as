package it.utag.controller 
{
	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.getQualifiedClassName;

	/**
	 * @author mikej
	 */
	public class Controller extends EventDispatcher
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(Controller);
		private var _subControllers : Array;
		private var _initingControllers : Array;

		public function Controller()
		{
			this._subControllers = [];
		}

		public function init() : void
		{
			this.initSubControllers();
		}

		public function addSubController(subController : Controller) : void
		{
			this._subControllers.push(subController);
		}

		public function initSubControllers() : void
		{
			this._initingControllers = _subControllers.concat();
			this.nextInit();
		}

		private function nextInit() : void
		{
			if(this._initingControllers.length == 0)
			{
				this.dispatchEvent(new Event(Event.INIT));
			}
			else
			{
				(this._initingControllers[0] as EventDispatcher).addEventListener(Event.INIT, this.handleSubControllerInit);
				_logger.debug("Init "+getQualifiedClassName((this._initingControllers[0] as Controller)));
				(this._initingControllers[0] as Controller).init();	
			}
		}

		private function handleSubControllerInit(event : Event) : void
		{
			(this._initingControllers[0] as EventDispatcher).removeEventListener(Event.INIT, this.handleSubControllerInit);
			this._initingControllers.shift();
			this.nextInit();
		}

		public function start() : void
		{
		}

		public function startSubControllers() : void
		{			
			// Restart any subcontrollers - they can now assume everything is ready.
			for each(var subController : Controller in this._subControllers)
			{
				_logger.debug("Start "+getQualifiedClassName(subController));
				subController.start();
			}
		}

		public function destroy() : void
		{
		}

		public function destroySubControllers() : void
		{
			// Reset any subcontrollers
			for each(var subController : Controller in this._subControllers)
			{
				_logger.debug("Destroy "+getQualifiedClassName(subController));
				subController.destroy();
			}
		}
	}
}
