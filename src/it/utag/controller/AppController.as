package it.utag.controller 
{
	import it.utag.controller.dialog.ImportController;
	import it.utag.controller.dialog.SearchController;
	import it.utag.controller.dialog.SwitchController;
	import it.utag.controller.event.UIEvent;
	import it.utag.controller.event.VideoEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.controller.manager.ServiceManager;
	import it.utag.gfxWindow;
	import it.utag.model.AppState;
	import it.utag.model.Tag;
	import it.utag.model.TagInstance;
	import it.utag.model.User;
	import it.utag.service.ServiceEvent;
	import it.utag.service.impl.TagListService;
	import it.utag.util.TimeUtil;

	import com.gskinner.motion.GTween;
	import com.gskinner.motion.GTweener;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.media.Video;
	import flash.text.TextField;

	/**
	 * @author mikej
	 */
	public class AppController extends Controller
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(AppController);

		private static var _instance : AppController;

		private var _user : User;
		private var _video : Video;

		private var _tagsController : TagsController;

		private var _sprite : Sprite;
		private var _window : gfxWindow;
		private var _videoController : VideoController;
		private var _addTagController : AddTagController;

		private var _tagListService : TagListService;
		private var _wavesController : WavesController;
		private var _waveTagsController : WaveTagsController;
		private var _saveController : SaveController;
		private var _cloudController : CloudController;

		// Buttons
		private var _cloudButton : MovieClip;
		private var _tagButton : SimpleButton;
		private var _snapshotButton : SimpleButton;
		private var _importButton : SimpleButton;
		private var _logInButton : SimpleButton;
		private var _logOutButton : SimpleButton;
		private var _searchButton : SimpleButton;
		private var _videosButton : SimpleButton;
		private var _importController : ImportController;
		private var _switchController : SwitchController;
		private var _glassPanel : MovieClip;
		private var _dialogPanel : MovieClip;
		private var _searchController : SearchController;
		private var _undoButton : SimpleButton;
		private var _glassTween : GTween;

		private var _firstTime : Boolean;
		private var _loadingPanel : MovieClip;

		public function AppController(sprite : Sprite = null)
		{
			_firstTime = true;
			if(sprite == null)
			{
				throw new IllegalOperationError("Can't call singleton constructor");
			}
			else
			{
				this._sprite = sprite;
				this._window = new gfxWindow();
				_instance = this;
			}
		}

		public function hideSaving() : void
		{
			this._window.saving.visible = false;
		}

		public function showSaving() : void
		{
			this._window.saving.visible = true;
		}

		public override function init() : void
		{
			this._sprite.addChild(this._window);	
			
			this._user = new User();
			this._video = new Video();

			this._glassPanel = this.createGlassPanel();
			this._dialogPanel = this.createDialogPanel();
			this._loadingPanel = this._window.loadingClip;
			this._loadingPanel.alpha = 1;
			
			this._sprite.addChild(this._glassPanel);
			this._sprite.addChild(this._dialogPanel);
			this._sprite.addChild(this._loadingPanel);
			

			
			this.initSubcontrollers();

			// Set up UI hooks

			this._tagButton = this._window.tagBtn;
			this._tagButton.addEventListener(MouseEvent.CLICK, this.handleTagClick);
			
			this._snapshotButton = this._window.curvesContainer["snapshot_btn"] as SimpleButton;
			this._snapshotButton.addEventListener(MouseEvent.CLICK, this.handleSnapshotClick);
			
			this._cloudButton = this._window.btnCloud;
			this._cloudButton.addEventListener(MouseEvent.CLICK, this.handleCloudClick);
			this._cloudButton.buttonMode = true;
			
			this._undoButton = this._window.curvesContainer["undo_btn"] as SimpleButton;
			this._undoButton.addEventListener(MouseEvent.CLICK, this.handleUndoClick);
			
			this._importButton = this._window.menuBar["btnImport"];
			this._logInButton = this._window.menuBar["btnLogIn"];
			this._logOutButton = this._window.menuBar["btnLogOut"];
			this._logOutButton.visible = false;
			this._videosButton = this._window.menuBar["btnVideos"];
			this._searchButton = this._window.menuBar["btnSearch"];
			
			this._importButton.addEventListener(MouseEvent.CLICK, this.handleImportClick);
			this._logInButton.addEventListener(MouseEvent.CLICK, this.handleLogInClick);
			this._logOutButton.addEventListener(MouseEvent.CLICK, this.handleLogOutClick);
			this._videosButton.addEventListener(MouseEvent.CLICK, this.handleVideosClick);
			this._searchButton.addEventListener(MouseEvent.CLICK, this.handleSearchClick);
			
			
			this._window.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.handleMouseMove);
			
			this._window.videoTimeCurves.mouseEnabled = false;
			this._window.videoTimeCurves.mouseChildren = false;
			this._window.scrubbar.mouseEnabled = false;
			this._window.scrubbar.mouseChildren = false;
			this._window.saving.visible = false;
			
			// Set up service listener
			ServiceManager.getInstance().addEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleServiceComplete);
			
			// Set up scrub-bar related events
			NotificationManager.getInstance().addEventListener(UIEvent.OVERLAY_SHOWN, this.handleOverlayShown);
			NotificationManager.getInstance().addEventListener(UIEvent.OVERLAY_HIDDEN, this.handleOverlayHidden);
			
			// Set up services, with their hooks to the app state if necessary.
			this._tagListService = new TagListService();
			this._tagListService.media = AppState.getInstance().media;
		
			this.dispatchEvent(new Event(Event.INIT));
		}

		private function handleUndoClick(event : MouseEvent) : void 
		{
			// A little hacky - as saving is timer-based, we don't
			// want to have to make the user wait when they press undo.
			if(this._wavesController.hasChange)
			{
				this._wavesController.saveChange();
			}
			
			if(AppState.getInstance().lastChangeItem)
			{
				this._saveController.undo();
			}
		}

		private function handleLogOutClick(event : MouseEvent) : void 
		{
			AppState.getInstance().user.username = User.ANON;
			AppController.getInstance().setLoggedIn(false);
			this.load();
		}

		private function handleSearchClick(event : MouseEvent) : void 
		{
			_searchController.show();
		}

		private function handleVideosClick(event : MouseEvent) : void 
		{
		}

		private function createDialogPanel() : MovieClip 
		{
			var dialogPanel : MovieClip = new MovieClip();
			//	dialogPanel.graphics.beginFill(0, 0);
			dialogPanel.graphics.drawRect(0, 0, _window.stage.width, _window.stage.height);
			//	dialogPanel.graphics.endFill();
			dialogPanel.mouseEnabled = true;
			return dialogPanel;
		}
		
		private function createGlassPanel() : MovieClip 
		{
			var glassPanel : MovieClip = new MovieClip();
			glassPanel.graphics.beginFill(0x000000, 0.4);
			glassPanel.graphics.drawRect(0, 0, _window.width, _window.height);
			glassPanel.graphics.endFill();
			glassPanel.visible = false;
			return glassPanel;
		}

		private function initSubcontrollers() : void
		{			
			// Set up controllers
			this._wavesController = new WavesController(this._window.curvesContainer, this._window.videoTimeCurves);
			this._videoController = new VideoController(this._window.videoPlayer, this._window.scrubbar);
			this._addTagController = new AddTagController(this._window.tagPanel);
			this._tagsController = new TagsController(this._window);
			this._waveTagsController = new WaveTagsController(this._window.waveTagGuide);
			this._cloudController = new CloudController(this._window.videoPlayer.cloudGuide, this._window.videoPlayer["seekbar_mc"]);
			this._saveController = new SaveController();
			this._importController = new ImportController(this._dialogPanel);
			this._switchController = new SwitchController(this._dialogPanel);
			this._searchController = new SearchController(this._dialogPanel, this._window.guidePanels);
			
			super.addSubController(this._videoController);
			super.addSubController(this._wavesController);
			super.addSubController(this._tagsController);
			super.addSubController(this._addTagController);
			super.addSubController(this._waveTagsController);
			super.addSubController(this._cloudController);
			super.addSubController(this._saveController);
			super.addSubController(this._importController);
			super.addSubController(this._switchController);
			super.addSubController(this._searchController);
		}

		private function handleOverlayHidden(event : UIEvent) : void 
		{
			if(event.modal)
				this.hideGlass();
			GTweener.to(this._window.scrubbar, 0.2, {alpha:1});
			GTweener.to(this._window.videoTimeCurves, 0.2, {alpha:1});
		}

		private function handleOverlayShown(event : UIEvent) : void 
		{
			if(event.modal)
				this.showGlass();
			GTweener.to(this._window.scrubbar, 0.2, {alpha:0});
			GTweener.to(this._window.videoTimeCurves, 0.2, {alpha:0});
		}

		/**
		 * This is simplest handled in this root controller, as both clips need
		 * to be synchronized.
		 */
		private function handleMouseMove(event : MouseEvent) : void 
		{
			if(	_window.videoPlayer.hitTestPoint(_window.stage.mouseX, _window.stage.mouseY, false) || (_wavesController && _wavesController.wavesView.hitTestPoint(_window.stage.mouseX, _window.stage.mouseY, false))
			)
			{
				var x : Number = _window.stage.mouseX;
				var seekBar : MovieClip = this._window.videoPlayer["seekbar_mc"] as MovieClip;
				var seekBarX : Point = this._window.videoPlayer.localToGlobal(new Point(seekBar.x, seekBar.y));
				
				if(x < seekBarX.x)
					x = seekBarX.x;
				if(x > seekBarX.x + seekBar.width - 5)
					x = seekBarX.x + seekBar.width - 5;
				this._window.videoTimeCurves.x = x;
				this._window.scrubbar.x = x;
		
				// Work out the position in ms
				var duration : Number = AppState.getInstance().media.duration;
				var instant : Number = ((x - seekBarX.x) / seekBar.width) * duration * 1000;
			
				(this._window.scrubbar["timecode"] as TextField).text = TimeUtil.formatTimePosition(instant);
				(this._window.videoTimeCurves["timecode"] as TextField).text = TimeUtil.formatTimePosition(instant);
			}
		}

		private function handleSnapshotClick(event : MouseEvent) : void 
		{
			this._waveTagsController.makeSnapshot();
		}

		private function handleTagClick(event : MouseEvent) : void
		{
			this._addTagController.togglePanel();
		}

		private function handleCloudClick(event : MouseEvent) : void
		{
			_cloudButton.alpha = (_cloudButton.alpha == 1 ? 0.5 : 1);
			this._cloudController.toggleCloud();
		}

		private function handleLogInClick(event : MouseEvent) : void 
		{
			this._switchController.show();
		}

		private function handleImportClick(event : MouseEvent) : void 
		{
			this._importController.show();
		}

		private function showGlass() : void
		{
			this._glassPanel.alpha = 0;
			this._glassPanel.visible = true;
			this._glassTween = GTweener.to(this._glassPanel, 0.2, {alpha:1});
		}

		private function hideGlass() : void
		{
			this._glassTween = new GTween(this._glassPanel, 0.2, {alpha:0});
			this._glassTween.dispatchEvents = true;
			this._glassTween.addEventListener(Event.COMPLETE, handleGlassHidden);
		}

		private function handleGlassHidden(event : Event) : void 
		{
			this._glassPanel.alpha = 0;
			this._glassPanel.visible = false;
		}

		public function load() : void
		{
			this._loadingPanel.mouseChildren = false;
			this._loadingPanel.visible = true;
			GTweener.to(this._loadingPanel, 0.5, {alpha:0.7, autoHide: true});
			//	this.showGlass();
			// First reset the model.
			AppState.getInstance().reset();
			// Reset all controllers
			if(!this._firstTime)
			{
				this.destroySubControllers();
			}
			_firstTime = false;
			// Load the tags
			_tagListService.media = AppState.getInstance().media;
			this.addEventListener(Event.INIT, this.handleControllersInit);
			ServiceManager.getInstance().sendRequest(_tagListService);
		}

		private function handleServiceComplete(event : ServiceEvent) : void
		{
			if(event.service == this._tagListService)
			{
				for each(var tagInstance : TagInstance in (this._tagListService.response.data as Array))
				{
					if(!AppState.getInstance().haveTag(tagInstance.tag.object))
					{
						AppState.getInstance().addTag(tagInstance.tag, false);
					}
					else
					{
						tagInstance.tag = AppState.getInstance().getTag(tagInstance.tag.object);
					}
				}
				
				// Updated tag list! Update the various views.
				AppState.getInstance().setTaggingEvents(this._tagListService.response.data as Array);
				
				
				// Get default 3 waves
				var waveTags : Array = AppState.getInstance().getHottestTags().slice(0, 3);
				
				for each(var tag : Tag in waveTags)
				{
					AppState.getInstance().addWave(tag);
				}
				
				this.addEventListener(Event.INIT, this.handleControllersInit);
				super.init();	
			}
		}

		private function handleControllersInit(event : Event) : void 
		{
			this.removeEventListener(Event.INIT, this.handleControllersInit);
			// Load up the video
			
			NotificationManager.getInstance().addEventListener(VideoEvent.LOADED, this.handleVideoLoaded);
			this._videoController.loadVideo(AppState.getInstance().media.url);
		}

		private function handleVideoLoaded(event : VideoEvent) : void 
		{
			
			NotificationManager.getInstance().removeEventListener(VideoEvent.LOADED, this.handleVideoLoaded);
			super.startSubControllers();
			
			var tween : GTween = GTweener.to(this._loadingPanel, 0.5, {alpha:0});
			tween.dispatchEvents = true;
			tween.addEventListener(Event.COMPLETE, this.handleLoadFadeComplete);
			
			this._window.username.text = AppState.getInstance().user.username;
		}

		private function handleLoadFadeComplete(event : Event) : void 
		{
			(event.target as GTween).removeEventListener(Event.COMPLETE, this.handleLoadFadeComplete);
			_logger.debug("Faded");
			this._loadingPanel.visible = false;
		}

		public override function start() : void
		{
			//http://www.youtube.com/watch?v=FArZxLj6DLk
			// bZBHZT3a-FA
			//	AppState.getInstance().media.url = "http://www.youtube.com/v/OM6XIICm_qo";
			AppState.getInstance().media.url = "http://www.youtube.com/v/iG9CE55wbtY";
			this.load();	
		}

		public static function getInstance() : AppController
		{
			if(_instance == null)
			{
				_instance = new AppController();
			}
			return _instance;
		}

		public function setLoggedIn(loggedIn : Boolean) : void 
		{
			this._logOutButton.visible = loggedIn;
			this._logInButton.visible = !loggedIn;
			this._window.username.text = AppState.getInstance().user.username;
		}
	}
}
