package it.utag.controller 
{
	import it.utag.controller.event.SnapshotEvent;
	import it.utag.controller.event.TagEvent;
	import it.utag.controller.event.TagInstanceEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.gfxTag;
	import it.utag.gfxWindow;
	import it.utag.model.AppState;
	import it.utag.model.Config;
	import it.utag.model.Datum;
	import it.utag.model.Media;
	import it.utag.model.Snapshot;
	import it.utag.model.Tag;
	import it.utag.view.slider.Slider;
	import it.utag.view.slider.SliderEvent;

	import com.gskinner.motion.GTween;

	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.utils.Timer;

	/**
	 * The TagController encapsulates tag type/ordering
	 * as well as scrolling/tag selection code.
	 * @author mikej
	 */
	public class TagsController extends Controller
	{
		private var _tagClips : Array;
		private var _tagContainer : MovieClip;

		// Scrolling components
		private var _scroller : MovieClip;
		private var _track : MovieClip;
		private var _mask : Sprite;
		private var _scrollbar : Slider;
		private var _upArrow : MovieClip;
		private var _downArrow : MovieClip;
		private var _scrollTimer : Timer;
		private var _scrollDirection : int;

		private var _tagListClip : MovieClip;
		private var _dropDownClip : MovieClip;
		private var _dropDownList : MovieClip;
		private var _clipData : Dictionary;

		private var _filter : uint;
		private var _sort : uint;
		private var _order : uint;
		private var _tagFilter : MovieClip;

		private var _btnName : MovieClip;
		private var _btnHotness : MovieClip;
		private var _btnDate : MovieClip;
		private var _btnNameOrder : MovieClip;
		private var _btnHotnessOrder : MovieClip;
		private var _btnDateOrder : MovieClip;
		private var _currentField : MovieClip;
		private var _txtFilter : TextField;

		
		public function TagsController(window : gfxWindow)
		{
			// Set up the dropdown
			this._dropDownClip = window.displayAllTags;
			this._dropDownList = window.rightSelection;
			
			this._dropDownList.visible = false;
			
			this._tagFilter = window.tagFilter;
			
			// Set up the tag list
			this._tagListClip = window.rhsTop;
			this._tagContainer = _tagListClip["tagContainer"] as MovieClip;
			this._scroller = _tagListClip["sb"]["scroller"] as MovieClip;
			this._track = _tagListClip["sb"]["track"] as MovieClip;
			this._upArrow = _tagListClip["sb"]["upArrow"] as MovieClip;
			this._downArrow = _tagListClip["sb"]["downArrow"] as MovieClip;
			this._scrollbar = new Slider(this._track, this._scroller, Slider.DIR_VERTICAL, 3, 6);
			
			this._btnName = createOverlayClip(this._tagFilter["btnName"]);
			this._btnHotness = createOverlayClip(this._tagFilter["btnHotness"]);
			this._btnDate = createOverlayClip(this._tagFilter["btnDate"]);
			
			
			this._btnNameOrder = this._tagFilter["btnNameOrder"] as MovieClip;
			this._btnHotnessOrder = this._tagFilter["btnHotnessOrder"] as MovieClip;
			this._btnDateOrder = this._tagFilter["btnDateOrder"] as MovieClip;
			
			this._btnNameOrder.visible = false;
			this._btnDateOrder.visible = false;
			
			//		this._btnAsc = this._tagFilter["btnAsc"] as MovieClip;
			//		this._btnDsc = this._tagFilter["btnDsc"] as MovieClip;
			//		this._txtSort = this._tagFilter["txtSort"] as TextField;
			this._txtFilter = window.displayAllTags["txtFilterMode"] as TextField;
			//		this._filterClip = this._tagFilter["filterClip"] as MovieClip;
			
			// Set up scrollmask			
			this._mask = new Sprite();
			this._mask.graphics.beginFill(0xFF0000);
			this._mask.graphics.drawRect(0, 0, Config.TAG_WIDTH, this._tagListClip.height);
			this._mask.graphics.endFill();
			this._tagListClip.addChild(this._mask);
			this._tagContainer.mask = this._mask;
		}

		private function createOverlayClip(field : DisplayObject) : MovieClip 
		{
			var newClip : MovieClip = new MovieClip();
			newClip.graphics.beginFill(0xff0000, 0);
			newClip.graphics.drawRect(0, 0, field.width, field.height);
			newClip.graphics.endFill();
			field.parent.addChild(newClip);
			newClip.x = field.x;
			newClip.y = field.y;
			return newClip;
		}

		public override function init() : void
		{

			this._tagClips = [];	
			
			this._upArrow.buttonMode = true;
			this._downArrow.buttonMode = true;
			this._scrollbar.addEventListener(SliderEvent.MOVED, this.handleSliderMoved);
			this._upArrow.addEventListener(MouseEvent.MOUSE_DOWN, this.handleScrollUpClick);
			this._downArrow.addEventListener(MouseEvent.MOUSE_DOWN, this.handleScrollDownClick);
			this._dropDownClip.addEventListener(MouseEvent.MOUSE_UP, this.handleMouseClick);
			
			this._btnDate.buttonMode = true;
			this._btnName.buttonMode = true;
			this._btnHotness.buttonMode = true;
			this._btnDateOrder.buttonMode = true;
			this._btnNameOrder.buttonMode = true;
			this._btnHotnessOrder.buttonMode = true;
			
			this._btnDate.addEventListener(MouseEvent.CLICK, this.handleFieldClicked);
			this._btnName.addEventListener(MouseEvent.CLICK, this.handleFieldClicked);
			this._btnHotness.addEventListener(MouseEvent.CLICK, this.handleFieldClicked);
			
			this._btnDateOrder.addEventListener(MouseEvent.CLICK, this.handleOrderClicked);
			this._btnNameOrder.addEventListener(MouseEvent.CLICK, this.handleOrderClicked);
			this._btnHotnessOrder.addEventListener(MouseEvent.CLICK, this.handleOrderClicked);
			//			this._btnDsc.addEventListener(MouseEvent.CLICK, this.handleOrderDescend);
			//			this._filterClip.addEventListener(MouseEvent.CLICK, this.handleSortTextClick);

			this._filter = AppState.TYPE_TAGS;
			this._sort = AppState.SORT_HOTTEST;
			this._order = AppState.ORDER_DESCENDING;
			this.updateFilterView();
			
			NotificationManager.getInstance().addEventListener(TagInstanceEvent.TAGINSTANCE_ADDED, this.handleTagInstanceEvent);
			NotificationManager.getInstance().addEventListener(TagInstanceEvent.TAGINSTANCE_REMOVED, this.handleTagInstanceEvent);
			NotificationManager.getInstance().addEventListener(SnapshotEvent.SNAPSHOT_ADDED, this.handleSnapshotAdded);
			NotificationManager.getInstance().addEventListener(TagInstanceEvent.TAGINSTANCE_UPDATED, this.handleTagInstanceEvent);
			NotificationManager.getInstance().addEventListener(TagEvent.TAG_ADDED, this.handleTagAdded);
			this.dispatchEvent(new Event(Event.INIT));
		}

		private function handleSnapshotAdded(event : SnapshotEvent) : void 
		{
			if(this._filter == AppState.TYPE_SNAPSHOTS)
			{
				this.redrawTagList();
			}
		}

		private function handleOrderClicked(event : MouseEvent) : void 
		{
		}

		private function handleFieldClicked(event : MouseEvent) : void 
		{
			if(event.target == _currentField)
			{
				this._order = (this._order == AppState.ORDER_ASCENDING ? AppState.ORDER_DESCENDING : AppState.ORDER_ASCENDING);
			}
			_currentField = event.target as MovieClip;
			
			if(event.target == _btnDate)
			{
				this._sort = AppState.SORT_MODIFIED;
			}
			else if(event.target == _btnName)
			{
				this._sort = AppState.SORT_ALPHA;
			}
			else if(event.target == _btnHotness)
			{
				this._sort = AppState.SORT_HOTTEST;
			}
			this.updateFilterView();
		}

		private function handleTagInstanceEvent(event : TagInstanceEvent) : void 
		{
			this.redrawTagList(event.type != TagInstanceEvent.TAGINSTANCE_UPDATED);
		}

		private function updateFilterView() : void
		{
			this._btnDateOrder.visible = false;
			this._btnHotnessOrder.visible = false;
			this._btnNameOrder.visible = false;
			
			var orderButton : MovieClip;
			
			switch(this._sort)
			{
				case AppState.SORT_HOTTEST:
					orderButton = this._btnHotnessOrder;
					break;
				case AppState.SORT_MODIFIED:
					orderButton = this._btnDateOrder;
					break;
				case AppState.SORT_ALPHA:
					orderButton = this._btnNameOrder;
					break;
			}
			
			orderButton.visible = true;
			switch(this._order)
			{
				case AppState.ORDER_ASCENDING:
					orderButton.gotoAndStop("up");
					break;
				case AppState.ORDER_DESCENDING:
					orderButton.gotoAndStop("down");
					break;
			}
			
			this.redrawTagList();
		}

		private function handleMouseClick(event : MouseEvent) : void 
		{
			if(!this._dropDownList.visible)
			{
				show();
			}
			else
			{
				hide();
			}
		}

		private function show() : void
		{
			this._dropDownList.alpha = 0;
			this._dropDownList.visible = true;
			
			// Enable buttons
			(this._dropDownList["btnVideoTags"] as SimpleButton).addEventListener(MouseEvent.CLICK, this.handleVideoTagsClicked);
			(this._dropDownList["btnUserTags"] as SimpleButton).addEventListener(MouseEvent.CLICK, this.handleUserTagsClicked);
			(this._dropDownList["btnSnapshots"] as SimpleButton).addEventListener(MouseEvent.CLICK, this.handleSnapshotsClicked);
			
			var tween : GTween = new GTween(this._dropDownList, 0.3, {alpha : 1});
			tween.onComplete = handleShown;
		}

		private function handleSnapshotsClicked(event : MouseEvent) : void 
		{
			this._filter = AppState.TYPE_SNAPSHOTS;
			this.hide();
			_txtFilter.text = "My Snapshots";
			this.redrawTagList();
		}

		private function handleUserTagsClicked(event : MouseEvent) : void 
		{
			this._filter = AppState.TYPE_USER_TAGS;
			this.hide();
			_txtFilter.text = "My Tags";
			this.redrawTagList();
		}

		private function handleVideoTagsClicked(event : MouseEvent) : void 
		{
			this._filter = AppState.TYPE_TAGS;
			this.hide();
			_txtFilter.text = "Tags in This Video";
			this.redrawTagList();
		}

		private function handleShown(tween : GTween) : void
		{
		}

		private function hide() : void
		{
			var tween : GTween = new GTween(this._dropDownList, 0.3, {alpha : 0});
			tween.onComplete = handleHidden;
			
			
			// Disable buttons
			(this._dropDownList["btnVideoTags"] as SimpleButton).removeEventListener(MouseEvent.CLICK, this.handleVideoTagsClicked);
			(this._dropDownList["btnUserTags"] as SimpleButton).removeEventListener(MouseEvent.CLICK, this.handleUserTagsClicked);
			(this._dropDownList["btnSnapshots"] as SimpleButton).removeEventListener(MouseEvent.CLICK, this.handleSnapshotsClicked);
		}

		private function handleHidden(tween : GTween) : void
		{
			this._dropDownList.visible = false;
		}

		private function handleScrollDownClick(event : MouseEvent) : void 
		{
			startScrolling(1);
		}

		private function handleScrollUpClick(event : MouseEvent) : void 
		{
			startScrolling(-1);
		}

		private function startScrolling(scrollDirection : int) : void
		{
			this._scrollDirection = scrollDirection;
			this._tagListClip.stage.addEventListener(MouseEvent.MOUSE_UP, this.handleScrollEnd);
			this._scrollTimer = new Timer(100);
			this._scrollTimer.addEventListener(TimerEvent.TIMER, this.handleScrollTimer);
			this._scrollTimer.start();
			handleScrollTimer(null);
		}

		private function handleScrollEnd(event : MouseEvent) : void 
		{
			this._scrollTimer.removeEventListener(TimerEvent.TIMER, this.handleScrollTimer);
			this._scrollTimer.stop();
		}

		private function handleScrollTimer(event : TimerEvent) : void 
		{
			// Height of one tag as a ratio of the whole height
			var scrollIncrement : Number = (this._tagContainer.height / this._tagClips.length) / (this._tagContainer.height - this._mask.height);
			this._scrollbar.position = (this._scrollbar.position + (_scrollDirection * scrollIncrement));		}

		private function handleSliderMoved(event : SliderEvent) : void 
		{
			// At 0, the top of the list is visible. At 100, the whole bottom of the list is visible.
			// Y can therefore go from 0 to (container.height-mask.height)
			var newPos : Number = (this._tagContainer.height - this._mask.height) * this._scrollbar.position;
			this._tagContainer.y = -newPos;
		}

		private function handleTagAdded(event : TagEvent) : void 
		{		
			this.redrawTagList();	
		}

		public override function start() : void
		{
			this.redrawTagList();
		}

		public override function destroy() : void
		{
			this.setUpVars();			
			
			this._scrollbar.removeEventListener(SliderEvent.MOVED, this.handleSliderMoved);
			this._upArrow.removeEventListener(MouseEvent.MOUSE_DOWN, this.handleScrollUpClick);
			this._downArrow.removeEventListener(MouseEvent.MOUSE_DOWN, this.handleScrollDownClick);
			this._dropDownClip.removeEventListener(MouseEvent.MOUSE_UP, this.handleMouseClick);
			//			this._btnAsc.removeEventListener(MouseEvent.CLICK, this.handleOrderAscend);
			//			this._btnDsc.removeEventListener(MouseEvent.CLICK, this.handleOrderDescend);
			//			this._filterClip.removeEventListener(MouseEvent.CLICK, this.handleSortTextClick);
			NotificationManager.getInstance().removeEventListener(TagInstanceEvent.TAGINSTANCE_ADDED, this.handleTagInstanceEvent);
			NotificationManager.getInstance().removeEventListener(TagInstanceEvent.TAGINSTANCE_REMOVED, this.handleTagInstanceEvent);
			NotificationManager.getInstance().removeEventListener(TagInstanceEvent.TAGINSTANCE_UPDATED, this.handleTagInstanceEvent);
			NotificationManager.getInstance().removeEventListener(TagEvent.TAG_ADDED, this.handleTagAdded);
		}

		private function setUpVars(scroll : Boolean = true) : void
		{
			if(scroll)
			{
				this._scrollbar.position = 0;
			}
			// Clear out any existing tags
			for each(var tagClip : gfxTag in this._tagClips)
			{
				this._tagContainer.removeChild(tagClip);
			}
			
			this._tagClips = [];
			this._clipData = new Dictionary();
		}

		public function redrawTagList(scroll : Boolean = true) : void 
		{
			this.setUpVars(scroll);
			
			// Then re-add
			var data : Array = AppState.getInstance().getData(this._filter, this._sort, this._order);
			
			for each (var datum:Datum in data)
			{
				var newTagClip : gfxTag = new gfxTag();
				newTagClip.tname.text = datum.name;
				newTagClip.txtModified.text = datum.modified;
				this._tagContainer.addChild(newTagClip);
				newTagClip.y = this._tagClips.length * newTagClip.height;
				
				var hitClip : MovieClip = new MovieClip();
				hitClip.graphics.beginFill(0, 0);
				hitClip.graphics.drawRect(0, 0, newTagClip.width, newTagClip.height);
				hitClip.graphics.endFill();
				hitClip.buttonMode = true;
				hitClip.addEventListener(MouseEvent.CLICK, this.handleDatumClick);
				newTagClip.addChild(hitClip);
				
				this._tagClips.push(newTagClip);
				this._clipData[hitClip] = datum;
			}
			
			if(this._scrollbar.position == 1 && scroll)
			{
				this._scrollbar.position = 1;
			}
			
			// Check to see if we need a scrollbar
			if(this._tagContainer.height > this._mask.height)
			{
				this._scroller.visible = true;
			}
			else
			{
				this._scroller.visible = false;
				this._scrollbar.position = 0;
			}
		}

		private function handleDatumClick(event : MouseEvent) : void 
		{
			var datum : Datum = (_clipData[event.target] as Datum);
			if(datum.type == Tag)
			{
				AppState.getInstance().addWave(AppState.getInstance().getTag(datum.name));
			}
			else if(datum.type == Media)
			{
				AppState.getInstance().reset();
				AppState.getInstance().media.url = datum.hidden;
				AppController.getInstance().load();
			}
			else if(datum.type == Snapshot)
			{
				AppState.getInstance().addWave(AppState.getInstance().getTag(datum.name));
			}
		}
	}
}
