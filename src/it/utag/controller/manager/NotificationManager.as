package it.utag.controller.manager 
{
	import flash.events.EventDispatcher;

	/**
	 * Acts as a central point for app-related
	 * notifications, easing use via a singleton accessor.
	 * @author mikej
	 */
	public class NotificationManager extends EventDispatcher
	{
		private static var _instance : NotificationManager;

		public static function getInstance() : NotificationManager
		{
			if(_instance == null)
			{
				_instance = new NotificationManager();
			}
			return _instance;
		}
	}
}
