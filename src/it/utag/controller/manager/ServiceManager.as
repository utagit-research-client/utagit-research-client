package it.utag.controller.manager 
{
	import it.utag.service.IService;
	import it.utag.service.Response;
	import it.utag.service.ServiceEvent;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;

	/**
	 * ServiceManager provides a singleton that handles
	 * dispatching a service and keeping track of the
	 * results. An IService implementation is used to 
	 * provide the URL, parameters, and to process the
	 * resultant data.
	 * 
	 * @author moj
	 */
	public class ServiceManager extends EventDispatcher
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(ServiceManager);
		private var _request : URLRequest;
		private var _response : Response;

		private static var _instance : ServiceManager;
		private var _serviceMap : Dictionary;

		public function ServiceManager()
		{
			this._serviceMap = new Dictionary();
		}

		public static function getInstance() : ServiceManager
		{
			if(_instance == null)
			{
				_instance = new ServiceManager();
			}
			return _instance;
		}

		
		public function sendRequest(service : IService) : void
		{
			
			this._request = new URLRequest(service.getURL());	
			this._request.data = service.buildRequest();	
			this._request.data["nocache"] = new Date().getTime();
			_logger.debug("Request sent: "+this._request.url+" "+this._request.data);
			var loader : URLLoader = new URLLoader();
			this._serviceMap[loader] = service;
			loader.addEventListener(Event.COMPLETE, this.handleRequestComplete);
			loader.load(this._request);
		}

		protected function handleRequestComplete(event : Event) : void
		{
			var loader : URLLoader = (event.target as URLLoader);
			loader.removeEventListener(Event.COMPLETE, this.handleRequestComplete);
			var service : IService = (this._serviceMap[loader] as IService);
			service.response = service.buildResponse(loader.data);
			
			delete this._serviceMap[loader];
			
			this.dispatchEvent(new ServiceEvent(ServiceEvent.SERVICE_COMPLETE, service));
		}

		public function get response() : Response
		{
			return _response;
		}

		public function set response(response : Response) : void
		{
			_response = response;
		}
	}
}
