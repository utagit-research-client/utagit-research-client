package it.utag.view.control 
{
	import it.utag.model.AppState;
	import it.utag.model.Config;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.Sprite;

	/**
	 * @author mikej
	 */
	public class VertexHandle extends Sprite 
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(VertexHandle);
		private var _selected : Boolean;
		private var _selectedGraphic : Sprite;
		private var _handleGraphic : Sprite;
		private var _type : uint;

		public function VertexHandle(type : uint)
		{
			super();
			
			this._type = type;
			this._handleGraphic = new Sprite();
			this.addChild(this._handleGraphic);
			this._selectedGraphic = new Sprite();
			this.addChild(this._selectedGraphic);

			this.updateGraphic();
		}

		private function updateGraphic() : void
		{
			var colour : uint = 0x000000;
		
			this._handleGraphic.graphics.clear();	
			this._handleGraphic.graphics.beginFill(colour, 0.3);
			this._handleGraphic.graphics.lineStyle(1, 0x000000);
			this._handleGraphic.graphics.drawCircle(0, 0, Config.VERTEX_RADIUS);
			this._handleGraphic.graphics.endFill();			
			
			this._selectedGraphic.graphics.clear();	
			this._selectedGraphic.graphics.beginFill(colour, 0.9);
			this._selectedGraphic.graphics.drawCircle(0, 0, Config.VERTEX_RADIUS - 3);
			this._selectedGraphic.graphics.endFill();
			this._selectedGraphic.visible = this._selected;
		}

		public function getRelevance() : Number
		{
			// TODO : Fix the height pad here.
			return 1 - (this.y / (Config.WAVE_HEIGHT));	
		}

		public function getInstant() : Number
		{
			var duration : Number = AppState.getInstance().media.duration;
			return Math.round((this.x / Config.WAVE_WIDTH) * duration * 1000);
		}

		public function get selected() : Boolean
		{
			return _selected;
		}

		public function set selected(selected : Boolean) : void
		{
			this._selectedGraphic.visible = selected;
			_selected = selected;
		}
		
		public function get type() : uint
		{
			return _type;
		}
		
		public function set type(type : uint) : void
		{
			_type = type;
		}
	}
}
