package it.utag.view 
{
	import it.utag.gfxAutoCompleteEntry;
	import it.utag.controller.manager.ServiceManager;
	import it.utag.service.ServiceEvent;
	import it.utag.service.impl.AutoCompleteService;

	import flash.display.MovieClip;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.utils.Dictionary;

	/**
	 * @author moj
	 */
	public class AutoCompleteField extends MovieClip 
	{
		private var _completeGuide : MovieClip;
		private var _textField : TextField;
		private var _autoCompleteService : AutoCompleteService;
		private var _results : Array;
		private var _hitClipTagNames : Dictionary;

		
		
		public function AutoCompleteField(textField : TextField, completeGuide : MovieClip)
		{
			this._completeGuide = completeGuide;
			this._textField = textField;
			this._autoCompleteService = new AutoCompleteService();
			this._hitClipTagNames = new Dictionary();
			this.enable();
		}

		public function enable() : void
		{
			ServiceManager.getInstance().addEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleServiceComplete);
			this._textField.addEventListener(KeyboardEvent.KEY_UP, this.handleTextInput);
		}

		public function disable() : void
		{
			ServiceManager.getInstance().removeEventListener(ServiceEvent.SERVICE_COMPLETE, this.handleServiceComplete);
			this._textField.removeEventListener(KeyboardEvent.KEY_UP, this.handleTextInput);
		}

		private function handleServiceComplete(event : ServiceEvent) : void
		{
			if(event.service == _autoCompleteService)
			{
				this._results = event.service.response.data as Array;	
				this._results.sort();
				this._results = this._results.slice(0, 5);	
				this.showResults();
			}
		}

		private function handleTextInput(event : KeyboardEvent) : void
		{
			this.reset();
			if(this._textField.text.length == 0)
			{
				return;
			}
			
			_autoCompleteService.predicate = "is_a";
			_autoCompleteService.tag_name = this._textField.text;
			ServiceManager.getInstance().sendRequest(_autoCompleteService);	
		}

		private function showResults() : void
		{
			
			
			for(var i : uint = 0;i < this._results.length;i++)
			{
				var tagName : String = this._results[i];
				var tagClip : gfxAutoCompleteEntry = new gfxAutoCompleteEntry();
				this._completeGuide.addChild(tagClip);
				tagClip.txtTag.text = tagName;
				tagClip.rollOverClip.visible = false;
				tagClip.y = (i * tagClip.height);
				tagClip.hitClip.useHandCursor = true;
				tagClip.hitClip.addEventListener(MouseEvent.CLICK, this.handleTagClick);
				tagClip.hitClip.addEventListener(MouseEvent.MOUSE_OVER, this.handleMouseOver);
				tagClip.hitClip.addEventListener(MouseEvent.MOUSE_OUT, this.handleMouseOut);
				this._hitClipTagNames[tagClip.hitClip] = tagName;
			}
		}

		private function handleMouseOut(event : MouseEvent) : void 
		{
			var tagClip : gfxAutoCompleteEntry = (event.target.parent as gfxAutoCompleteEntry);
			
			tagClip.rollOverClip.visible = false;
		}

		private function handleMouseOver(event : MouseEvent) : void 
		{
			var tagClip : gfxAutoCompleteEntry = (event.target.parent as gfxAutoCompleteEntry);
			tagClip.rollOverClip.visible = true;
		}

		private function handleTagClick(event : MouseEvent) : void
		{
			this._textField.text = (_hitClipTagNames[event.target]);
			this.reset();
		}

		private function reset() : void
		{
			this._hitClipTagNames = new Dictionary();
			if(this._completeGuide.numChildren == 0)
			{
				return;
			}	
			
			
			while(this._completeGuide.numChildren > 0)
			{
				this._completeGuide.removeChildAt(0);
			}
		}
	}
}
