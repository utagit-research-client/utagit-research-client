package it.utag.view 
{
	import it.utag.model.AppState;
	import it.utag.model.Config;
	import it.utag.model.Tag;
	import it.utag.model.TagInstance;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.LineScaleMode;
	import flash.display.Sprite;
	import flash.geom.Point;

	/**
	 * @author mikej
	 */
	public class WaveView extends Sprite
	{
		public static const TYPE_TAG : uint = 1;
		public static const TYPE_SNAPSHOT : uint = 2;

		private var _logger : ILogger = LoggerFactory.getClassLogger(WaveView);

		private var _colour : uint;
		private var _selected : Boolean;
		private var _tag : Tag;
		private var _tagView : Sprite;
		private var _snapView : Sprite;

		public function WaveView(tag : Tag, colour : uint)
		{
			this._tag = tag;
			this._colour = colour;
			
			this._tagView = new Sprite();
			this._snapView = new Sprite();
			this._snapView.alpha = 0.5;
			
			this.addChild(this._snapView);
			this.addChild(this._tagView);
		}

		public function toggleSnapshotView(value : Object = null) : void
		{
			this.redrawSnapshot();
			
			if(value == null)
			{
				this._snapView.visible = !this._snapView.visible;
			}
			else if(value == true || value == false)
			{
				this._snapView.visible = (value as Boolean);
			}
			
			_logger.debug("Visible: " + this._snapView.visible);
		}

		private function drawGraph(target : Object, instances : Array) : void
		{
		
			var width : Number = this.parent.width;
			var height : Number = this.parent.height - Config.WAVE_PAD;
			var duration : Number = AppState.getInstance().media.duration;

			// Remove any deleted instances
			for each(var instance : TagInstance in instances.concat())
			{
				if(instance.type == TagInstance.TYPE_DELETED)
				{
					instances.splice(instances.indexOf(instance), 1);
				}
			}

			if(instances.length == 0)
			{
				return;
			}
			
			// We add on a vertex at the start and end, based on the spec.

			if(instances[0].instant > 0)
			{
				var ti1 : TagInstance = new TagInstance();
				ti1.type = TagInstance.TYPE_DEFAULT;
				ti1.instant = (instances[0] as TagInstance).instant - (10000 % (duration * 1000));
				if(ti1.instant < 0) ti1.instant = 0;
				ti1.relevance = 0;
				instances.unshift(ti1);
			}
					
			if(instances[instances.length - 1].instant < (duration * 1000))
			{
				var ti2 : TagInstance = new TagInstance();
				ti2.type = TagInstance.TYPE_DEFAULT;
				ti2.instant = (instances[instances.length - 1] as TagInstance).instant + (10000 % (duration * 1000));
				if(ti2.instant > (duration * 1000)) ti2.instant = (duration * 1000);
				ti2.relevance = 0;
				instances.push(ti2);
			}
			
			var firstTime : Boolean = true;
			for(var i : uint = 0;i < instances.length - 1;i++)
			{
				var i1 : TagInstance = (instances[i] as TagInstance);
				var i2 : TagInstance = (instances[i + 1] as TagInstance);
				if(i1.type == TagInstance.TYPE_DELETED || i2.type == TagInstance.TYPE_DELETED)
				{
					// Plotting a deleted instance!
				}
				var v1 : Point = new Point(i1.instant, i1.relevance);
				var v2 : Point = new Point(i2.instant, i2.relevance);
				for(var x : Number = v1.x;x <= v2.x;x += 100)
				{
					
					var y : Number = ((v1.y + v2.y) / 2) + (((v1.y - v2.y) / 2) * Math.cos((Math.PI * (x - v1.x)) / (v2.x - v1.x)));

					var pixX : Number = (width / duration) * (x / 1000.0);
					var pixY : Number = (1.0 - y) * height;
					if(firstTime)
					{
						target.moveTo(pixX, pixY);
						firstTime = false;
					}
					else
					{
						target.lineTo(pixX, pixY);	
					}
				}
			}
		}

		public function redrawSnapshot() : void
		{
			var cloned : Array = [];
			cloned = (AppState.getInstance().getSnapshotInstancesForTag(_tag).sortOn("instant", Array.NUMERIC) as Array).concat();
			//cloned = (AppState.getInstance().getTagInstancesForTag(_tag).sortOn("instant", Array.NUMERIC) as Array).concat();

			_snapView.graphics.clear();
			if(cloned.length == 0)
			{
				_logger.debug("No instances");
				return;
			}
			this._snapView.graphics.lineStyle((_selected ? 4 : 2), this._colour, (_selected ? 1 : 0.5), false, LineScaleMode.NORMAL);
			
			drawGraph(_snapView.graphics, cloned);
		}

		
		public function redrawTag() : void
		{
			var cloned : Array = [];		
			cloned = (AppState.getInstance().getTagInstancesForTag(_tag).sortOn("instant", Array.NUMERIC) as Array).concat();
	
			this._tagView.graphics.clear();
			if(cloned.length == 0)
			{
				_logger.debug("No instances");
				return;
			}
			this._tagView.graphics.lineStyle((_selected ? 4 : 2), this._colour, (_selected ? 1 : 0.5), false, LineScaleMode.NORMAL);
			drawGraph(this._tagView.graphics, cloned);
		}

		public function get selected() : Boolean
		{
			return _selected;
		}

		public function set selected(selected : Boolean) : void
		{
			_selected = selected;
			this.redrawTag();
			this.redrawSnapshot();
		}

		public function set colour(colour : uint) : void
		{
			_colour = colour;
			this.redrawTag();
			this.redrawSnapshot();
		}

		public function get tagView() : Sprite
		{
			return _tagView;
		}

		public function get snapView() : Sprite
		{
			return _snapView;
		}
	}
}
