package it.utag.view 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	/**
	 * @author moj
	 */
	public class CloudTextField extends MovieClip
	{
		private var _textField : TextField;
		private var _format : TextFormat;

		public function CloudTextField()
		{
			this._textField = new TextField();
			this._textField.autoSize = TextFieldAutoSize.CENTER;
			this._textField.scaleX = 0;
			this._textField.scaleY = 0;
			this._textField.selectable = false;
			this._textField.mouseEnabled = false;
			
			
			this._format = new TextFormat();
			_format.font = "Arial";
			_format.size = 36;
			_format.color = 0xffffff;
			_format.align = TextFormatAlign.CENTER;
					
			this._textField.setTextFormat(_format);
			this._textField.text = "";
			this.addChild(_textField);
		}
		
		public function setTextFormat(format : TextFormat) : void
		{
			if(format != null)
			{
				_textField.setTextFormat(format);
				this._format = format;
				_textField.x = -(_textField.width/2);
				_textField.y = (_textField.height/2);
			}
		}
		
		public function set text(_text : String) : void
		{
			_textField.text = _text;
			this.setTextFormat(_format);
		}
		
		public function get text() : String
		{
			return _textField.text;
		}
		
		public override function set scaleX(_scaleX : Number) : void
		{
			_textField.scaleX = _scaleX;
			_textField.x = -(_textField.width/2);
			_textField.y = (_textField.height/2);
		}

		public override function set scaleY(_scaleY : Number) : void
		{
			_textField.scaleY = _scaleY;
			_textField.x = -(_textField.width/2);
			_textField.y = (_textField.height/2);
		}
		
		public override function get scaleY() : Number
		{
			return _textField.scaleY;
		}
		
		public override function get scaleX() : Number
		{
			return _textField.scaleX;
		}
	}
}
