package it.utag.view 
{
	import it.utag.controller.event.VertexEvent;
	import it.utag.controller.manager.NotificationManager;
	import it.utag.model.AppState;
	import it.utag.model.Config;
	import it.utag.model.TagInstance;
	import it.utag.view.control.VertexHandle;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * @author mikej
	 */
	public class WavesView extends Sprite
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(WavesView);
		private var _waveContainer : Sprite;

		private var _controls : Array;
		private var _controlsContainer : Sprite;
		private var _selectedHandle : VertexHandle;

		
		public function WavesView()
		{
			this._controls = [];
			
			this._waveContainer = new Sprite();
			this._controlsContainer = new Sprite();
			this.addChild(this._waveContainer);
			this._waveContainer.graphics.lineStyle(1, 0x00ffff, 0);
			this._waveContainer.graphics.drawRect(0, 0, Config.WAVE_WIDTH, Config.WAVE_HEIGHT);
			
			this._waveContainer.x = 20;
			this.addChild(this._controlsContainer);
			this._controlsContainer.graphics.lineStyle(1, 0x00ffff, 0);
			this._controlsContainer.graphics.drawRect(-Config.VERTEX_RADIUS, -Config.VERTEX_RADIUS, Config.WAVE_WIDTH + (Config.VERTEX_RADIUS * 2), Config.WAVE_HEIGHT + (Config.VERTEX_RADIUS * 2));
			this._controlsContainer.x = 20;
		}

		public function clearControls() : void
		{
			_logger.debug("Controls: "+this._controls.length);
			for each(var vertexHandle : VertexHandle in _controls)
			{
				vertexHandle.selected = false;
				this._controlsContainer.removeChild(vertexHandle);
			}
			this._controls = [];
			this._selectedHandle = null;
		}

		public function addControl(taggingEvent : TagInstance) : VertexHandle
		{
			var duration : Number = AppState.getInstance().media.duration;
			var vertexHandle : VertexHandle = new VertexHandle(taggingEvent.type);
			
			vertexHandle.x = (_waveContainer.width / duration) * (taggingEvent.instant / 1000.0);
			vertexHandle.y = ((1.0 - taggingEvent.relevance) * (_waveContainer.height - Config.WAVE_PAD));	

			vertexHandle.buttonMode = true;
			vertexHandle.mouseChildren = false;
			vertexHandle.addEventListener(MouseEvent.MOUSE_DOWN, this.handleControlClick);
			this._controlsContainer.addChild(vertexHandle);
			this._controls.push(vertexHandle);
			
			_logger.debug("Control count: "+this._controls.length);
			
			
			return vertexHandle;
		}
		
		public function positionVertexHandle(vertexHandle : VertexHandle, tagInstance : TagInstance) : void
		{
			var duration : Number = AppState.getInstance().media.duration;
			vertexHandle.x = (_waveContainer.width / duration) * (tagInstance.instant / 1000.0);
			vertexHandle.y = ((1.0 - tagInstance.relevance) * (_waveContainer.height - Config.WAVE_PAD));	
			
		}

		private function handleControlClick(event : MouseEvent) : void 
		{
			_logger.debug("Control click");
			if(_selectedHandle)
			{
				_logger.debug("Deselect "+_selectedHandle);
				_selectedHandle.selected = false;
				this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.handleControlMove);
			}
			
			_selectedHandle = (event.target as VertexHandle);
			_selectedHandle.selected = true;
			
			_logger.debug("Selected handle now "+_selectedHandle);
			this.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.handleControlMove);
			this.stage.addEventListener(MouseEvent.MOUSE_UP, this.handleControlRelease);
			var rect : Rectangle = this._controlsContainer.getBounds(this._controlsContainer);

			rect.y += Config.VERTEX_RADIUS;
			rect.height -= (Config.VERTEX_RADIUS * 2);
			rect.x += Config.VERTEX_RADIUS;
			rect.width -= (Config.VERTEX_RADIUS * 2);
			_selectedHandle.startDrag(false, rect);
			NotificationManager.getInstance().dispatchEvent(new VertexEvent(VertexEvent.VERTEX_GRABBED, _selectedHandle));
		}

		private function handleControlRelease(event : MouseEvent) : void 
		{
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, this.handleControlRelease);
			this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.handleControlMove);
			_selectedHandle.stopDrag();
			NotificationManager.getInstance().dispatchEvent(new VertexEvent(VertexEvent.VERTEX_RELEASED, _selectedHandle));
		}

		private function handleControlMove(event : MouseEvent) : void 
		{
			var relPos : Point = this._controlsContainer.globalToLocal(new Point(event.stageX, event.stageY));
			
			if(relPos.x >= _controlsContainer.width || relPos.x < 0 || relPos.y >= _controlsContainer.height || relPos.y < 0)
			{
				if(_selectedHandle.type != TagInstance.TYPE_DELETED)
				{
					_selectedHandle.type = TagInstance.TYPE_DELETED;
					_selectedHandle.visible = false;
				}
			}	
			else
			{
				if(_selectedHandle.type != TagInstance.TYPE_DEFAULT)
				{
					_selectedHandle.type = TagInstance.TYPE_DEFAULT;
					_selectedHandle.visible = true;
				}
			}
			NotificationManager.getInstance().dispatchEvent(new VertexEvent(VertexEvent.VERTEX_MOVED, _selectedHandle));
		}

		public function addWaveView(waveView : WaveView) : void
		{
			this._waveContainer.addChild(waveView);
		}

		public function removeWaveView(waveView : WaveView) : void
		{
			this._waveContainer.removeChild(waveView);
		}

		public function bringToFront(selectedWaveView : WaveView) : void 
		{
			this._waveContainer.setChildIndex(selectedWaveView, this._waveContainer.numChildren - 1);
		}

		public function get waveContainer() : Sprite
		{
			return _waveContainer;
		}
		
		public function set selectedHandle(selectedHandle : VertexHandle) : void
		{
			_selectedHandle = selectedHandle;
		}
	}
}
