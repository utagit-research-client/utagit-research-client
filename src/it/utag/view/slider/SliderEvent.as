package it.utag.view.slider 
{
	import flash.events.Event;

	/**
	 * @author moj
	 */
	public class SliderEvent extends Event 
	{
		public static const MOVED : String = "moved";
		
		public function SliderEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone() : Event
		{
			return new SliderEvent(this.type, this.bubbles, this.cancelable);
		}
	}
}
