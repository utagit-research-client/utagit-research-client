package it.utag.view.slider 
{
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	/**
	 * @author moj
	 */
	public class Slider extends EventDispatcher
	{
		public static const DIR_HORIZONTAL : uint = 0;
		public static const DIR_VERTICAL : uint = 1;

		private var _container : MovieClip;
		
		private var _slider : MovieClip;
		private var _position : Number;
		private var _rectangle : Rectangle;
		private var _direction : uint;
		private var _padding1 : Number;
		private var _padding2 : Number;

		public function Slider(container : MovieClip, slider : MovieClip, direction : uint = DIR_HORIZONTAL, padding1 : Number = 0, padding2 : Number = 0)
		{
			this._container = container;
			this._slider = slider;
			
			this._slider.buttonMode = true;
			this._container.buttonMode = true;
			
			this._direction = direction;
			this._padding1 = padding1;
			this._padding2 = padding2;
			
			if(_direction == DIR_HORIZONTAL)
			{
				this._rectangle = new Rectangle(this._container.x, this._container.y, this._container.width, 0);
			
				//	 this._container.getBounds(this._container.parent);
				_rectangle.x += (this._slider.width / 2);
				_rectangle.width -= this._slider.width;
			}
			else if(_direction == DIR_VERTICAL)
			{
				this._rectangle = new Rectangle(this._slider.x, this._container.y, 0, this._container.height);
				this._rectangle.y += _padding1;
				this._rectangle.height -= _padding2;
				this._rectangle.height -= this._slider.height;
				
				// Avoid sub-pixel co-ords
				this._rectangle.y = Math.round(this._rectangle.y);
				this._rectangle.height = Math.round(this._rectangle.height);
				
				this._slider.y = this._container.y + _padding1;
			}
			
			this._position = 0;
			
			this._slider.addEventListener(MouseEvent.MOUSE_DOWN, this.handleMouseDown);
		}

		private function handleMouseUp(event : MouseEvent) : void
		{
			this._slider.stage.removeEventListener(MouseEvent.MOUSE_UP, this.handleMouseUp);
			this._slider.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.handleMouseMove);
			this._slider.addEventListener(MouseEvent.MOUSE_DOWN, this.handleMouseDown);
			this._slider.stopDrag();
		}

		private function handleMouseMove(event : MouseEvent) : void
		{
			if(this._direction == DIR_HORIZONTAL)
			{
				this._position = (this._slider.x - this._rectangle.x) / this._rectangle.width;
			}
			else
			{
				this._position = (this._slider.y - this._rectangle.y) / this._rectangle.height;
			}
			this.dispatchEvent(new SliderEvent(SliderEvent.MOVED));
		}
		
		public function set position(_position: Number) : void
		{
			// Cap the input _position
			if(_position > 1) _position = 1;
			if(_position < 0) _position = 0;
			
			this._position = _position;
			if(this._direction == DIR_VERTICAL)
			{
				this._slider.y = (this._position * this._rectangle.height) + this._rectangle.y;
			}
			else
			{
				this._slider.x = (this._position * this._rectangle.width) + this._rectangle.x;
			}
			this.dispatchEvent(new SliderEvent(SliderEvent.MOVED));
		}

		private function handleMouseDown(event : MouseEvent) : void
		{
			this._slider.removeEventListener(MouseEvent.MOUSE_DOWN, this.handleMouseDown);
			this._slider.startDrag(true, this._rectangle);
			this._slider.stage.addEventListener(MouseEvent.MOUSE_UP, this.handleMouseUp);
			this._slider.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.handleMouseMove);
		}

		public function get position() : Number
		{
			return _position;
		}
	}
}
