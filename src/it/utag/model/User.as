package it.utag.model 
{
	import it.utag.controller.event.SnapshotEvent;
	import it.utag.controller.manager.NotificationManager;

	import flash.utils.Dictionary;

	/**
	 * @author mikej
	 */
	public class User 
	{
		public static const ANON : String = "anonymous";
		
		private var _username : String;
		private var _snapshots : Dictionary;
		
		public function User()
		{
			this._username = ANON;
			this._snapshots = new Dictionary();
		}
		
		public function get username() : String
		{
			return _username;
		}
		
		public function set username(username : String) : void
		{
			_username = username;
		}
		
		public function get snapshots() : Dictionary
		{
			return _snapshots;
		}
		
		public function set snapshots(snapshots : Dictionary) : void
		{
			_snapshots = snapshots;
		}
		
		public function addSnapshot(snapshot : Snapshot) : void
		{
			_snapshots[snapshot.tag] = snapshot;
			NotificationManager.getInstance().dispatchEvent(new SnapshotEvent(SnapshotEvent.SNAPSHOT_ADDED, snapshot));
		}
	}
}
