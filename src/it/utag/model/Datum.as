package it.utag.model 
{

	/**
	 * @author moj
	 */
	public class Datum 
	{
		private var _type : Class;
		private var _name : String;
		private var _modified : String;
		private var _added : String;
		private var _hidden : String;
		
		public function Datum()
		{
			this._name = "";
			this._modified = "";
			this._hidden = "";
		}
		
		public function get type() : Class
		{
			return _type;
		}
		
		public function set type(type : Class) : void
		{
			_type = type;
		}
		
		public function get name() : String
		{
			return _name;
		}
		
		public function set name(name : String) : void
		{
			_name = name;
		}
		
		public function get modified() : String
		{
			return _modified;
		}
		
		public function set modified(modified : String) : void
		{
			_modified = modified;
		}
		
		public function get hidden() : String
		{
			return _hidden;
		}
		
		public function set hidden(hidden : String) : void
		{
			_hidden = hidden;
		}
		
		public function toString() : String
		{
			return "[Datum name="+name+" modified="+modified+" hidden="+hidden+" type="+type+"]";
		}
		
		public function get added() : String
		{
			return _added;
		}
		
		public function set added(added : String) : void
		{
			_added = added;
		}
	}
}
