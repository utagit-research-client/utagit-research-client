package it.utag.model 
{

	/**
	 * @author mikej
	 */
	public class Snapshot 
	{
		private var _tag : Tag;
		private var _instances : Array;
		private var _creation : Date;
		
		public function get tag() : Tag
		{
			return _tag;
		}
		
		public function set tag(tag : Tag) : void
		{
			_tag = tag;
		}
		
		public function get instances() : Array
		{
			return _instances;
		}
		
		public function set instances(instances : Array) : void
		{
			_instances = instances;
		}
		
		public function get creation() : Date
		{
			return _creation;
		}
		
		public function set creation(creation : Date) : void
		{
			_creation = creation;
		}
		
		public function toString() : String
		{
			return "[Snapshot tag="+tag+" creation="+creation+" instances="+instances+"]";
		}
	}
}
