package it.utag.model 
{

	/**
	 * @author mikej
	 */
	public class Config 
	{
		// The colours to use when showing graphs (order important).
		public static const WAVE_COLOURS : Array = [0x00acbe, 0x7200db, 0xed0008, 0x5ee62b, 0xe3900c];
		
		// A small vertical margin is necessary to allow for bold waves (as they may edge over the height
		// of the container clip).
		public static const WAVE_PAD : uint = 4;
		
		// The radius of the vertex handles on an editable curve.
		public static const VERTEX_RADIUS : uint = 7;
		
		// The width/height of a wave canvas in pixels.
		public static const WAVE_WIDTH : uint = 565;
		public static const WAVE_HEIGHT : uint = 159;
		
		public static const MAX_WAVES : uint = 5;
		
		public static const VIDEO_WIDTH : uint = 585
		public static const VIDEO_HEIGHT : uint = 329;
		
		
		public static const TAG_WIDTH : uint = 305;
	}
}
