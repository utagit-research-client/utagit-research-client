package it.utag.model 
{

	/**
	 * @author mikej
	 */
	public class Media 
	{
		private var _url : String;
		private var _duration : Number;
		private var _position : Number;
		private var _title : String;
		private var _thumbURL : String;

		public function get url() : String
		{
			return _url;
		}
		
		public function set url(url : String) : void
		{
			_url = url;
		}
		
		public function get duration() : Number
		{
			return _duration;
		}
		
		public function set duration(duration : Number) : void
		{
			_duration = duration;
		}
		
		public function get position() : Number
		{
			return _position;
		}
		
		public function set position(position : Number) : void
		{
			_position = position;
		}
		
		public function get title() : String
		{
			return _title;
		}
		
		public function set title(title : String) : void
		{
			_title = title;
		}
		
		public function get thumbURL() : String
		{
			return _thumbURL;
		}
		
		public function set thumbURL(thumbURL : String) : void
		{
			_thumbURL = thumbURL;
		}
	}
}
