package it.utag.model 
{
	import it.utag.controller.event.SaveEvent;
	import it.utag.controller.event.TagEvent;
	import it.utag.controller.event.TagInstanceEvent;
	import it.utag.controller.event.WaveEvent;
	import it.utag.controller.event.WaveTagEvent;
	import it.utag.controller.manager.NotificationManager;

	import org.as3commons.logging.ILogger;
	import org.as3commons.logging.LoggerFactory;

	import flash.geom.Point;
	import flash.utils.Dictionary;

	/**
	 * This is a singleton used to store the current
	 * state of the utagit app. It contains the current
	 * tag dictionary (ensuring that predicate/object combos
	 * get reused) and the current media which is being
	 * tagged.
	 * 
	 * @author moj
	 */
	public class AppState 
	{
		private var _logger : ILogger = LoggerFactory.getClassLogger(AppState);

		// Singleton instance
		private static var _instance : AppState;

		public static const SORT_HOTTEST : uint = 0;
		public static const SORT_ALPHA : uint = 1;
		public static const SORT_MODIFIED : uint = 2;

		public static const TYPE_TAGS : uint = 0;
		public static const TYPE_MOVIES : uint = 1;
		public static const TYPE_USER_TAGS : uint = 2;
		public static const TYPE_SNAPSHOTS : uint = 3;

		public static const ORDER_ASCENDING : uint = 0;
		public static const ORDER_DESCENDING : uint = 1;

		private var _sortType : uint;
		private var _sortOrder : uint;

		// Current video
		private var _media : Media;

		// Generic array for sorting.
		private var _tags : Array;

		// Mapping from object to Tag
		private var _tagDictionary : Dictionary;

		// The waves currently being viewed in the tag timeline.
		private var _waves : Array;

		// Mapping from Tag to array of TagInstances
		private var _tagInstances : Dictionary;

		private var _user : User;

		private var _changeItems : Array;
		private var _lastChangeItem : ChangeItem;

		public function AppState()
		{
			this._tagDictionary = new Dictionary();
			this._tagInstances = new Dictionary();
			this._tags = [];
			this._changeItems = [];
			this._user = new User();
			
			this._sortType = SORT_HOTTEST;
			this._sortOrder = ORDER_ASCENDING;
			
			this._waves = [];
			this._media = new Media();
			this._media.url = "http://www.youtube.com/watch?v=u1zgFlCw8Aw";
		}

		public function clearUndo() : void
		{
			_changeItems = [];
			_lastChangeItem = null;
		}

		public function removeChangeItem(topmost : ChangeItem) : void
		{
			if(changeItems.indexOf(topmost) != -1)
			{
				changeItems.splice(changeItems.indexOf(topmost), 1);
			}
			else
			{
				_logger.debug("Couldn't remove changeitem");
			}
		}

		public function getTagInstanceAt(tag : Tag, instant : uint) : TagInstance
		{
			var tagInstances : Array = this.getTagInstancesForTag(tag);
			for each(var instance : TagInstance in tagInstances)
			{
				if(instance.instant == instant)
				{
					return instance;
				}
			}
			return null;
		}

		public function getTagInstancesAt(position : uint) : Array
		{
			var instances : Array = [];
			var duration : Number = AppState.getInstance().media.duration;
			for each(var tag : Tag in this._tags)
			{
				var tagInstances : Array = this.getTagInstancesForTag(tag);
				//			if(tagInstances.length == 0) continue;
				var cloned : Array = tagInstances.concat().sortOn("instant", Array.NUMERIC) as Array;
				
				// We add on a vertex at the start and end, based on the spec.
				if(cloned[0].instant > 0)
				{
					var ti1 : TagInstance = new TagInstance();
					ti1.instant = (cloned[0] as TagInstance).instant - (10000 % (duration * 1000));
					if(ti1.instant < 0) ti1.instant = 0;
					ti1.relevance = 0;
					cloned.unshift(ti1);
				}
			
				if(cloned[cloned.length - 1].instant < (duration * 1000))
				{
					var ti2 : TagInstance = new TagInstance();
					ti2.instant = (cloned[cloned.length - 1] as TagInstance).instant + (10000 % (duration * 1000));
					if(ti2.instant > (duration * 1000)) ti2.instant = (duration * 1000);
					ti2.relevance = 0;
					cloned.push(ti2);
				}
				 
				 
				
				for(var i : uint = 0;i < cloned.length - 1;i++)
				{
					var i1 : TagInstance = (cloned[i] as TagInstance);
					var i2 : TagInstance = (cloned[i + 1] as TagInstance);
					var v1 : Point = new Point(i1.instant, i1.relevance);
					var v2 : Point = new Point(i2.instant, i2.relevance);
					if(i1.instant <= position && position <= i2.instant)
					{
						// In the right range.
						var relevance : Number = ((v1.y + v2.y) / 2) + (((v1.y - v2.y) / 2) * Math.cos((Math.PI * (position - v1.x)) / (v2.x - v1.x)));
						
						var fakeInstance : TagInstance = new TagInstance();
						fakeInstance.instant = position;
						fakeInstance.relevance = relevance;
						fakeInstance.tag = tag;
						instances.push(fakeInstance);
						
						break;
					}
				}
			}
			
			return instances;
		}

		public function addChangeItem(changeItem : ChangeItem) : void
		{
			this._lastChangeItem = changeItem.clone();
			this._changeItems.push(changeItem);
			
			NotificationManager.getInstance().dispatchEvent(new SaveEvent(SaveEvent.STATE_DIRTY));
		}

		public static function getInstance() : AppState
		{
			if(_instance == null)
			{
				_instance = new AppState();
			}
			return _instance;
		}

		
		/**
		 * Tag handling
		 */

		public function haveTag(name : String) : Boolean
		{
			return (_tagDictionary[name] != null);
		}

		public function getTag(name : String) : Tag
		{
			return _tagDictionary[name];	
		}

		/**
		 * Add a tag to the application state. If isNew is true,
		 * the tag is completely new to the system. If false,
		 * we're just loading it in.
		 */
		public function addTag(tag : Tag, isNew : Boolean = true) : void
		{
			if(!haveTag(tag.object))
			{
				_tagDictionary[tag.object] = tag;
				_tags.push(tag);
				_tagInstances[tag] = [];
				
				NotificationManager.getInstance().dispatchEvent(new TagEvent(TagEvent.TAG_ADDED, tag, isNew));
			}
		}

		public function removeTag(tag : Tag) : void
		{
			if(haveTag(tag.object))
			{
				delete _tagDictionary[tag.object];
				delete _tagInstances[tag];
				_tags.splice(_tags.indexOf(tag), 1);
			}
		}

		/**
		 * TaggingEvent handling
		 */

		public function setTaggingEvents(tagInstances : Array) : void
		{
			this._tagInstances = new Dictionary();
			
			for each(var instance : TagInstance in tagInstances)
			{
				addTagInstance(instance);
			}
		}

		public function addTagInstance(tagInstance : TagInstance) : void
		{
			this.addTag(tagInstance.tag);
			
			if(!_tagInstances[tagInstance.tag])
				_tagInstances[tagInstance.tag] = [];
		
			
			//	_logger.debug("Add instance for "+tagInstance.tag.object+" at "+tagInstance.instant);

			(this._tagInstances[tagInstance.tag] as Array).push(tagInstance);
			
			
			NotificationManager.getInstance().dispatchEvent(new TagInstanceEvent(TagInstanceEvent.TAGINSTANCE_ADDED, tagInstance));
		}

		public function removeTagInstance(tagInstance : TagInstance) : void
		{
			if(_tagInstances[tagInstance.tag])
			{
				var events : Array = _tagInstances[tagInstance.tag];
				for (var i : uint = 0;i < events.length;i++)
				{
					if(events[i].instant == tagInstance.instant && events[i].relevance == tagInstance.relevance)
					{
						events.splice(i, 1);
					}
				}
				if(events.length == 0)
				{
					delete _tagInstances[tagInstance.tag];
					this.removeTag(tagInstance.tag);
				}
				
				NotificationManager.getInstance().dispatchEvent(new TagInstanceEvent(TagInstanceEvent.TAGINSTANCE_REMOVED, tagInstance));
			}
		}

		public function getTagInstancesForTag(tag : Tag) : Array
		{
			var results : Array = [];
			for each(var instance : TagInstance in _tagInstances[tag])
			{
				results.push(instance);	
			}
			return results;
		}

		public function getSnapshotInstancesForTag(tag : Tag) : Array
		{
			if(!this._user.snapshots[tag])
			{
				return [];
			}
			return (this._user.snapshots[tag] as Snapshot).instances;
		}

		/**
		 * Utility methods
		 */

		public function getHottestTags() : Array
		{
			var tagCache : Array = this._tags.concat();
			tagCache.sortOn("weight", Array.NUMERIC | Array.DESCENDING);
			return tagCache;
		}

		public function addWave(tag : Tag) : void
		{
			if(this._waves.indexOf(tag) == -1)
			{
				if(this._waves.length == Config.MAX_WAVES)
				{
					NotificationManager.getInstance().dispatchEvent(new WaveEvent(WaveEvent.WAVE_REMOVED, this._waves.shift()));
				}
				this._waves.push(tag);
				NotificationManager.getInstance().dispatchEvent(new WaveEvent(WaveEvent.WAVE_ADDED, tag));
			}
			else
			{
				
				NotificationManager.getInstance().dispatchEvent(new WaveTagEvent(WaveTagEvent.WAVETAG_SELECTED, tag));
			}
		}

		public function haveWave(tag : Tag) : Boolean
		{
			return (this._waves.indexOf(tag) != -1);
		}

		public function removeWave(tag : Tag) : void
		{
			if(this._waves.indexOf(tag) != -1)
			{
				this._waves.splice(this._waves.indexOf(tag), 1);
				NotificationManager.getInstance().dispatchEvent(new WaveEvent(WaveEvent.WAVE_REMOVED, tag));
			}
		}

		/*
		 * Returns a list of Datum objects.
		 */
		public function getData(type : uint = TYPE_TAGS, sort : uint = SORT_HOTTEST, order : uint = ORDER_DESCENDING ) : Array
		{
			var results : Array = [];
			
			
			if(type == TYPE_SNAPSHOTS)
			{
				for each(var snapshot : Snapshot in this.user.snapshots)
				{
					var snapshotDatum : Datum = new Datum();
					snapshotDatum.type = Snapshot;
					snapshotDatum.name = snapshot.tag.object;
					snapshotDatum.modified = snapshot.tag.modified_on.toLocaleString();
					snapshotDatum.added = snapshot.tag.added_on.toLocaleString();
					results.push(snapshotDatum);
				}
				return results;
			}
			
			var allTags : Array = this.tags.concat();
			if(sort == SORT_HOTTEST)
			{
				allTags.sortOn("weight", (order == ORDER_DESCENDING ? Array.DESCENDING : null));
			}
			else if(sort == SORT_ALPHA)
			{
				allTags.sortOn("object", (order == ORDER_DESCENDING ? Array.DESCENDING : null));
			}
			else if(sort == SORT_MODIFIED)
			{
				allTags.sortOn("modified_on", (order == ORDER_DESCENDING ? Array.DESCENDING : null));
			}
			
			if(type == TYPE_TAGS)
			{
				// Default : All tags in this media
				for each(var tag : Tag in allTags)
				{
					var datum : Datum = new Datum();
					datum.type = Tag;
					datum.name = tag.object;
					datum.modified = tag.modified_on.toLocaleString();	
					datum.added = tag.added_on.toLocaleString();
					results.push(datum);
				}
			}
			else if(type == TYPE_USER_TAGS)
			{
				for each(var userTag : Tag in allTags)
				{
					if(userTag.contributors.indexOf(this.user.username) == -1)
					{
						continue;
					}
					
					var userTagDatum : Datum = new Datum();
					userTagDatum.type = Tag;
					userTagDatum.name = userTag.object;
					userTagDatum.modified = userTag.modified_on.toLocaleString();	
					userTagDatum.added = userTag.added_on.toLocaleString();
					results.push(userTagDatum);
				}
			}
			return results;		
		}

		/**
		 * Accessors
		 */

		public function get media() : Media
		{
			return _media;
		}

		public function set media(media : Media) : void
		{
			_media = media;
		}

		public function get waves() : Array
		{
			return _waves;
		}

		public function get tags() : Array
		{
			return _tags;
		}

		public function get sortOrder() : uint
		{
			return _sortOrder;
		}

		public function set sortOrder(sortOrder : uint) : void
		{
			_sortOrder = sortOrder;
		}

		public function get sortType() : uint
		{
			return _sortType;
		}

		public function set sortType(sortType : uint) : void
		{
			_sortType = sortType;
		}

		public function get user() : User
		{
			return _user;
		}

		public function get changeItems() : Array
		{
			return _changeItems;
		}

		public function reset() : void 
		{			
			this._tagDictionary = new Dictionary();
			this._tagInstances = new Dictionary();
			this._tags = [];
			this._changeItems = [];
			
			this._sortType = SORT_HOTTEST;
			this._sortOrder = ORDER_ASCENDING;
			
			this._waves = [];
			this.user.snapshots = new Dictionary();
		}

		public function get lastChangeItem() : ChangeItem
		{
			return _lastChangeItem;
		}
	}
}
