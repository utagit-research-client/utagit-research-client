package it.utag.model 
{

	/**
	 * @author moj
	 */
	public class ChangeItem 
	{
		public static const TYPE_ADD : String = "add";
		public static const TYPE_DELETE : String = "delete";
		public static const TYPE_UPDATE : String = "update";
//		public static const TYPE_MOVE : String = "move";
		
		private var _oldTagInstance : TagInstance;
		private var _newTagInstance : TagInstance;
		private var _type : String;

		public function get type() : String
		{
			return _type;
		}
		
		public function set type(type : String) : void
		{
			_type = type;
		}
		
		public function get oldTagInstance() : TagInstance
		{
			return _oldTagInstance;
		}
		
		public function set oldTagInstance(oldTagInstance : TagInstance) : void
		{
			_oldTagInstance = oldTagInstance;
		}
		
		public function get newTagInstance() : TagInstance
		{
			return _newTagInstance;
		}
		
		public function set newTagInstance(newTagInstance : TagInstance) : void
		{
			_newTagInstance = newTagInstance;
		}
		
		public function equals(changeItem : ChangeItem) : Boolean
		{
			if(changeItem.type != this.type)
			{
				return false;
			}
			if(changeItem.newTagInstance != this.newTagInstance)
			{
				return false;
			}
			if(changeItem.oldTagInstance != this.oldTagInstance)
			{
				return false;
			}
			return true;
		}

		public function toString() : String
		{
			return "[HistoryItem type="+type+"]";//+" newTagInstance="+newTagInstance+" oldTagInstance="+oldTagInstance+"]";
		}
		
		public function clone() : ChangeItem
		{
			var _clone : ChangeItem = new ChangeItem();
			if(this._oldTagInstance)
			{
				_clone.oldTagInstance = this._oldTagInstance.clone();
			}
			if(this._newTagInstance)
			{
				_clone.newTagInstance = this._newTagInstance.clone();
			}
			_clone.type = this._type;
			return _clone;
		}
	}
}
