package it.utag.model 
{

	/**
	 * @author mikej
	 */
	public class TagInstance 
	{
		
		public static const DOD_DOT : String = "dot";
		public static const DOD_DASH : String = "dash";
		
		public static const TYPE_DEFAULT : uint = 0;
		public static const TYPE_DELETED : uint = 1;
		
		private var _instant : Number;
		private var _relevance : Number;
		private var _dod : String;
		private var _tag : Tag;
		private var _media : Media;
		private var _type : uint;
		private var _creator : String;

		public function TagInstance()
		{
			this._dod = DOD_DOT;
			this._type = TYPE_DEFAULT;
		}
		
		public function get instant() : Number
		{
			return _instant;
		}
		
		public function set instant(instant : Number) : void
		{
			_instant = instant;
		}
		
		public function get relevance() : Number
		{
			return _relevance;
		}
		
		public function set relevance(relevance : Number) : void
		{
			_relevance = relevance;
		}
		
		public function get tag() : Tag
		{
			return _tag;
		}
		
		public function set tag(tag : Tag) : void
		{
			_tag = tag;
		}
		
		public function toString() : String
		{
			return "[TagInstance instant="+_instant+", relevance="+_relevance+" tag="+tag+", dod="+_dod+", type="+_type+" media="+_media+", creator="+_creator+"]";
		}
		
		public function get dod() : String
		{
			return _dod;
		}
		
		public function set dod(dod : String) : void
		{
			_dod = dod;
		}
		
		public function get media() : Media
		{
			return _media;
		}
		
		public function set media(media : Media) : void
		{
			_media = media;
		}
		
		public function clone() : TagInstance
		{
			var instance : TagInstance = new TagInstance();
			instance.dod =  this.dod;
			instance.instant = this.instant;
			instance.media = this.media;
			instance.relevance = this.relevance;
			instance.tag = this.tag;
			instance.creator = this.creator;
			return instance;	
		}
		
		public function get type() : uint
		{
			return _type;
		}
		
		public function set type(type : uint) : void
		{
			_type = type;
		}
		
		public function get creator() : String
		{
			return _creator;
		}
		
		public function set creator(creator : String) : void
		{
			_creator = creator;
		}
	}
}
