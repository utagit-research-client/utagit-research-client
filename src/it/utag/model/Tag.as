package it.utag.model 
{

	/**
	 * @author mikej
	 */
	public class Tag 
	{
		private var _predicate : String;
		private var _object : String;
		private var _added_on : Date;
		private var _modified_on : Date;

		public function get predicate() : String
		{
			return _predicate;
		}

		public function set predicate(predicate : String) : void
		{
			_predicate = predicate;
		}

		public function get object() : String
		{
			return _object;
		}

		public function set object(object : String) : void
		{
			_object = object;
		}

		public function toString() : String
		{
			return "[Tag predicate = " + predicate + ", object = " + object+", added = " + _added_on + ", modified = "+_modified_on +", weight = "+weight+", contributors = "+this.contributors+"]";
		}
		
		/**
		 * Weight is generated dynamically from the summation of relevances
		 * raised to the power of their position in descending order.
		 */
		public function get weight() : Number
		{
			// Get all weights for this tag
			var tagInstances : Array = AppState.getInstance().getTagInstancesForTag(this);
			if(!tagInstances || tagInstances.length == 0)
			{
				return 0;
			}
			
			var relevances : Array = [];
			for each(var taggingEvent : TagInstance in tagInstances)
			{
				relevances.push(taggingEvent.relevance);
			}
			
			relevances.sort(Array.NUMERIC | Array.DESCENDING);
			
			var _weight : Number = 0;
			for(var i:uint=0; i<relevances.length; i++)
			{
				_weight += Math.pow(relevances[i], i+1);
			}
			return _weight;	
		}
		
		public function get added_on() : Date
		{
			return _added_on;
		}
		
		public function set added_on(added_on : Date) : void
		{
			_added_on = added_on;
		}
		
		public function get contributors() : Array
		{
			// Get instances
			var instances : Array = AppState.getInstance().getTagInstancesForTag(this);
			var _contributors : Array = [];
			for each(var instance : TagInstance in instances)
			{
				if(_contributors.indexOf(instance.creator) == -1)
				{
					_contributors.push(instance.creator);
				}
			}
			return _contributors;
		}
		
		public function get modified_on() : Date
		{
			return _modified_on;
		}
		
		public function set modified_on(modified_on : Date) : void
		{
			_modified_on = modified_on;
		}
	}
}
